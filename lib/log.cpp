﻿#include "log.h"

dSFML::Lib::DebugStream* dSFML::Lib::DebugStream::DebugStr = nullptr;

dSFML::Lib::DebugStream::DebugStream(string logpath, bool logdate, bool logtostdout, bool logtofile){
    setp(0,0); //overflow bitch!
    DebugStr = this;
    File.open(logpath, ios::app | ios::out);  //Still ANSI. Waiting for codecvt header
    LogDate = logdate; LogToConsole = logtostdout; LogToFile = logtofile;
}

dSFML::Lib::DebugStream::~DebugStream(){ DebugStr = nullptr; if(!NewLineStarted) WriteChar('\n'); if(File.is_open()) File.close(); }
int dSFML::Lib::DebugStream::overflow(int c){ WriteChar((char)c); return c; }

void dSFML::Lib::DebugStream::Log(string s){ for(auto i = s.begin(); i != s.end(); i++) WriteChar(*i); }
void dSFML::Lib::DebugStream::LogLine(string s){ s += '\n'; Log(s); }
void dSFML::Lib::DebugStream::LogFormated(string s, ...){ va_list vlist; va_start(vlist, s); LogFormatedV(s, vlist); va_end(vlist); }
void dSFML::Lib::DebugStream::LogFormatedV(string s, va_list vlist){
    char* buf = (char*)malloc(sizeof(char)*4096);
    vsprintf(buf, s.c_str(), vlist);
    while(*buf != 0) WriteChar(*buf++); free(buf);
}

void dSFML::Lib::DebugStream::PrintHeader(){
    char* buf = (char*)malloc(sizeof(char)*64); NewLineStarted = false;
    time_t cT; struct tm* lT;
    time(&cT); lT = localtime(&cT);
    if(LogDate) sprintf(buf, "[%02d.%02d.%04d-%02d:%02d:%02d]: ", lT->tm_mday, lT->tm_mon+1, lT->tm_year+1900, lT->tm_hour, lT->tm_min, lT->tm_sec);
    else sprintf(buf, "[%02d:%02d:%02d]: ", lT->tm_hour, lT->tm_min, lT->tm_sec);
    while(*buf != 0) WriteChar(*buf++); free(buf);
}

void dSFML::Lib::DebugStream::WriteChar(char c){ WriteWChar((wchar_t)c); }
void dSFML::Lib::DebugStream::WLog(wstring s){ for(auto i = s.begin(); i != s.end(); i++) WriteWChar(*i); }
void dSFML::Lib::DebugStream::WLogLine(wstring s){ s += '\n'; WLog(s); }
void dSFML::Lib::DebugStream::WLogFormated(wstring s, ...){ va_list vlist; va_start(vlist, s); WLogFormatedV(s, vlist); va_end(vlist); }
void dSFML::Lib::DebugStream::WLogFormatedV(wstring s, va_list vlist){
    wchar_t* buf = (wchar_t*)malloc(sizeof(wchar_t)*4096);
    vswprintf(buf, s.c_str(), vlist);
    while(*buf != 0) WriteWChar(*buf++); free(buf);
}

void dSFML::Lib::DebugStream::WriteWChar(wchar_t c){
    if(NewLineStarted)PrintHeader();
    if(LogToFile && File.is_open()) { File<<c; File.flush(); }
    if(LogToConsole) wcout<<c;
    if(DebugConsole != nullptr) DebugConsole->AppendChar(c);
    if(c == '\n') NewLineStarted = true;
}

void dSFML::Lib::DebugStream::ConnectConsole(Console* DbCon){ DebugConsole = DbCon; }
void dSFML::Lib::DebugStream::Write(string s){ if(DebugStr != nullptr) DebugStr->Log(s); }
void dSFML::Lib::DebugStream::WriteL(string s){ if(DebugStr != nullptr) DebugStr->LogLine(s); }
void dSFML::Lib::DebugStream::WriteF(string s, ...){ if(DebugStr != nullptr){ va_list vlist; va_start(vlist, s); DebugStr->LogFormatedV(s, vlist); va_end(vlist); } }
void dSFML::Lib::DebugStream::WriteW(wstring s){ if(DebugStr != nullptr) DebugStr->WLog(s); }
void dSFML::Lib::DebugStream::WriteWL(wstring s){ if(DebugStr != nullptr) DebugStr->WLogLine(s); }
void dSFML::Lib::DebugStream::WriteWF(wstring s, ...){ if(DebugStr != nullptr){ va_list vlist; va_start(vlist, s); DebugStr->WLogFormatedV(s, vlist); va_end(vlist); } }
dSFML::Lib::DebugStream* dSFML::Lib::DebugStream::GetDebugStr() { return DebugStr; }
