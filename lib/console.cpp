#include "console.h"

dSFML::Lib::Console::Console(){
    LineBuf.push_back(new wstring(L"")); IStr = L"";
}

dSFML::Lib::Console::~Console(){
    if(TextBuf.size() != 0) for(auto i = TextBuf.begin(); i != TextBuf.end(); i++) delete *i;
    if(LineBuf.size() != 0) for(auto i = LineBuf.begin(); i != LineBuf.end(); i++) delete *i;
}

void dSFML::Lib::Console::Init(){

}

void dSFML::Lib::Console::SetGame(Game* gg){
    g = gg; ini.SetUnicode(true); ini.LoadFile(g->l.c_str());
    if(!g->PTextures->GetFont("fonts/consolefont.ttf", &CFont)){ dSFML::Lib::DebugStream::WriteL("FATAL ERROR: File [ Asserts\\Texturs.dsaf ]\"\\fonts/consolefont.ini\" not found!"); throw dSFML::Lib::Exception(dSFML::Lib::Exception::EType::FileLoad); }
    OffsetInfo.setFont(CFont); OffsetInfo.setString("Y-offset: %d\nX-offset: %d"); OffsetInfo.setCharacterSize(12); OffsetInfo.setColor(sf::Color::Yellow);
    IText.setFont(CFont); IText.setString(L"Test"); IText.setCharacterSize(12); IText.setColor(sf::Color(0xF3, 0xF3, 0xF3));
    ith = IText.getLocalBounds().height; clk.restart();
}

void dSFML::Lib::Console::Events(sf::Event e){
    if(e.type == sf::Event::KeyPressed){
        if(e.key.code == sf::Keyboard::PageUp) { if(pointer < maxp) { pointer++; Changed = true; } }
        if(e.key.code == sf::Keyboard::PageDown) { if(pointer > 1) { pointer--; Changed = true; } }
        if(e.key.code == sf::Keyboard::Home) { if(xoffset > xmax + xstep) { xoffset-=xstep; Changed = true; } }
        if(e.key.code == sf::Keyboard::End) { if(xoffset < 0) { xoffset+=xstep; Changed = true; } }
        if(e.key.code == sf::Keyboard::Return) { if(IStr != L"") { ProceedCommand(IStr); IStr = L""; } }
        if(e.key.code == sf::Keyboard::BackSpace) { if(IStr != L"") { IStr.pop_back(); } }
    }
    if(e.type == sf::Event::TextEntered){
        int n = e.text.unicode;
        wchar_t c = (wchar_t)n;
        //dSFML::Lib::DebugStream::WriteWF(L"Entered [char]: %c; Code [int] = %d\n", c, n);
        if(c != '`' && n > 31 && n < 128) IStr += c;
    }
}

void dSFML::Lib::Console::ProceedCommand(wstring cmd){
    dSFML::Lib::DebugStream::WriteWL(L"> " + cmd);
    vector<wstring> args; wstring tmp = L""; bool ignorespace = false;
    for(auto i = cmd.begin(); i != cmd.end(); i++){
        wchar_t ch = *i; if(ch != ' ' && ch != '"') tmp += ch;
        if(ch == ' ' && !ignorespace) { args.push_back(tmp); tmp = L""; }
        if(ch == ' ' && ignorespace) tmp += ch;
        if(ch == '"') ignorespace = !ignorespace;
    } args.push_back(tmp);

    if(g->SendCommand(args));
    else dSFML::Lib::DebugStream::WriteWL(dSFML::Lib::GetLangString(&ini, "console", "unknowcmd"));
}

void dSFML::Lib::Console::Draw(sf::RenderWindow* w){
    sf::Vector2u v = w->getSize(); int maxw = 0;
    if(v != LastSize || Changed){
        float b = (float)v.y * 0.4f; bool lasttime = false;
        if(TextBuf.size() != 0) for(auto i = TextBuf.begin(); i != TextBuf.end(); i++) delete *i; TextBuf.clear();
        for(auto i = LineBuf.rbegin() + pointer; i != LineBuf.rend(); i++){
            sf::Text* t = new sf::Text; t->setFont(CFont); t->setString(sf::String(**i)); t->setCharacterSize(12);
            t->setColor(sf::Color(0xA9,0xAD,0xA8)); b -= t->getLocalBounds().height + interline; t->setPosition(xoffset, b);
            TextBuf.push_back(t); if(lasttime) break; if(b < 0) lasttime = true;
            if(maxw < t->getLocalBounds().width) maxw = t->getLocalBounds().width;
        }
        Changed = false; xmax = -maxw;
    }
    LastSize = v;
    //drawing
    sf::RectangleShape rect(sf::Vector2f((float)v.x, (float)v.y * 0.4f)); rect.setFillColor(sf::Color(0x46, 0x46, 0x46, 192));
    sf::RectangleShape inputrect(sf::Vector2f((float)v.x, ith + 4*interline)); inputrect.setFillColor(sf::Color(0x5E, 0x5E, 0x5E, 192)); inputrect.setPosition(0, (float)v.y * 0.4f);
    w->draw(rect);
    w->draw(inputrect);
    for(auto i = TextBuf.begin(); i != TextBuf.end(); i++) w->draw(**i);
    //offsetinfo postition
    OffsetInfo.setPosition(v.x - OffsetInfo.getLocalBounds().width, 0);
    char buf[64]; sprintf(buf, "Y-offset: %d\nX-offset: %d", pointer-1, -xoffset); OffsetInfo.setString(sf::String(buf));
    w->draw(OffsetInfo);
    if(clk.getElapsedTime().asMilliseconds() > 500) { clk.restart(); drawsp = !drawsp; }
    if(drawsp) IText.setString(sf::String(IStr + L"_"));
    else IText.setString(sf::String(IStr));
    IText.setPosition(0, (float)v.y * 0.4f + interline);
    w->draw(IText);
}

void dSFML::Lib::Console::AppendChar(wchar_t c){
    Changed = true;
    if(c == '\n') { LineBuf.push_back(new wstring(L"")); maxp++; if(LineBuf.size() > bufsize) { delete *(LineBuf.begin()); LineBuf.erase(LineBuf.begin()); maxp--; } }
    else *(LineBuf.back()) += c;
}
