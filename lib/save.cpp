#include "save.h"

dSFML::Lib::SaveFile::SaveFile(string f){ FileName = f; }

vector<dSFML::Lib::SaveFile*> dSFML::Lib::SaveFile::GetSaves(){
    DIR* dir; struct dirent* ent; vector<dSFML::Lib::SaveFile*> ret; map<long int, dSFML::Lib::SaveFile*> m;

    /*if((dir = opendir("Save")) != NULL){ while((ent = readdir(dir)) != NULL) { if(ent->d_type == DT_REG) {
            if(dSFML::Lib::has_suffix(string(ent->d_name), ".dssf")){
                SaveFile* sav = dSFML::Lib::SaveFile::LoadSave("Save\\" + string(ent->d_name));
                if(sav != 0) ret.push_back(sav);
            }
    } } closedir(dir); }*/

    if((dir = opendir("Save")) != NULL){ while((ent = readdir(dir)) != NULL) { if(ent->d_type == DT_REG) { if(dSFML::Lib::has_suffix(string(ent->d_name), ".dssf")){
        struct stat attrib; stat(("Save\\" + string(ent->d_name)).c_str(), &attrib); time_t modtime = attrib.st_mtime;
        SaveFile* sav = dSFML::Lib::SaveFile::LoadSave("Save\\" + string(ent->d_name));
        m.insert(pair<long int, dSFML::Lib::SaveFile*>(static_cast<long int>(modtime), sav));
    }}} closedir(dir); }
    else mkdir("Save");
    for(auto i = m.rbegin(); i != m.rend(); i++)ret.push_back(i->second);
    return ret;
}

string dSFML::Lib::SaveFile::GetNewSaveName(){
    long int t = static_cast<long int>(time(NULL));
    ostringstream ss; ss<<t; string n = ss.str();
    if(!dSFML::Lib::fexists("Save/" + n + ".dssf")) return "Save/" + n + ".dssf";
    for(int i = 0; true ; i++) { ss.str(""); ss<<i; string t = "-" + ss.str(); if(!dSFML::Lib::fexists("Save/" + n + t + ".dssf")) return "Save/" + n + t + ".dssf"; }
}

dSFML::Lib::SaveFile* dSFML::Lib::SaveFile::CreateNewSave(string f, sf::Image img){
    string path = GetNewSaveName(); mz_bool s;
    //if(!s){ dSFML::Lib::DebugStream::WriteL("Failed to save \"" + path + "\" save file! #S1"); return nullptr; }
    {
        //create metainfo
        CSimpleIniW metaini; metaini.SetUnicode(true); string inibuf; char* buf; size_t bufsize;
        metaini.SetValue(L"save", L"name", wstring(f.begin(), f.end()).c_str());
        metaini.SetValue(L"save", L"quest", L"tutorial1");
        metaini.SetLongValue(L"save", L"diff", 0);
        metaini.SetLongValue(L"save", L"lvl", 0);
        metaini.Save(inibuf); buf = (char*)inibuf.c_str(); bufsize = inibuf.length();
        s = mz_zip_add_mem_to_archive_file_in_place(path.c_str(), "metainfo.ini", buf, bufsize, NULL, 0, MZ_BEST_COMPRESSION);
        if(!s){ dSFML::Lib::DebugStream::WriteL("Failed to save \"" + path + "\" save file! #S1"); return nullptr; }
    }
    {
        //create pic
        if(img.saveToFile("pic.png")){
            fstream file; file.open("pic.png", ios_base::in | ios_base::binary);
            if(file){
                file.seekg(0, file.end); int len = file.tellg(); file.seekg(0, file.beg);
                char* buf = new char[len]; file.read(buf, len); file.close();
                mz_zip_add_mem_to_archive_file_in_place(path.c_str(), "pic.png", buf, (size_t)len, NULL, 0, MZ_BEST_COMPRESSION);
            }
            else dSFML::Lib::DebugStream::WriteL("Failed to save \"" + path + "\" save file! #S3");
        }
        else dSFML::Lib::DebugStream::WriteL("Failed to save \"" + path + "\" save file! #S2");
        remove("pic.png");
    }
    return LoadSave(path);
}

dSFML::Lib::SaveFile* dSFML::Lib::SaveFile::LoadSave(string path){
    mz_zip_archive zipsave; int numofreqfiles = 0;
    memset(&zipsave, 0, sizeof(zipsave));
    mz_bool s; s = mz_zip_reader_init_file(&zipsave, path.c_str(), 0);
    if(!s){ dSFML::Lib::DebugStream::WriteL("Failed to open \"" + path + "\" save file! #C1"); return nullptr; }
    SaveFile* sav = new SaveFile(path);
    sav->Path = path;
    for(int i = 0; i < (int)mz_zip_reader_get_num_files(&zipsave); i++){
        mz_zip_archive_file_stat fstat; if(!mz_zip_reader_file_stat(&zipsave, i, &fstat)){
             dSFML::Lib::DebugStream::WriteL("Failed to open \"" + path + "\" save file! #C2"); delete sav; mz_zip_reader_end(&zipsave); return nullptr;
        }
        if(!strcmp(fstat.m_filename, "metainfo.ini")){
            //handle metainfo
            numofreqfiles++;
            size_t psize; void* p = mz_zip_reader_extract_to_heap(&zipsave, i, &psize, 0);
            CSimpleIniW metaini; metaini.SetUnicode(true);
            SI_Error rc = metaini.LoadData((const char*)p, psize);
            if(rc < 0) { dSFML::Lib::DebugStream::WriteL("Failed to open \"" + path + "\" save file! #C4"); delete sav; mz_zip_reader_end(&zipsave); return nullptr; }
            sav->SaveName = metaini.GetValue(L"save", L"name", L"noname");
            wstring quest_t = metaini.GetValue(L"save", L"quest", L"noquest");
            sav->Quest = string(quest_t.begin(), quest_t.end());
            sav->Diff = (int)metaini.GetLongValue(L"save", L"diff", 0);
            sav->Level = (int)metaini.GetLongValue(L"save", L"lvl", 0);
            mz_free(p);
        }
        if(!strcmp(fstat.m_filename, "pic.png")){
            //handle save picture
            size_t psize; void* p = mz_zip_reader_extract_to_heap(&zipsave, i, &psize, 0);
            if(!sav->Pic.loadFromMemory((const void*)p, psize+1)) { sav->IsPic = false; dSFML::Lib::DebugStream::WriteL("Failed to open screenshot in \"" + path + "\" save file! #C5"); }
            else { sav->Pic = dSFML::Lib::ResizeImageBilinear(sav->Pic, 240, -1); sav->IsPic = true; }
            mz_free(p);
        }
    }
    if(numofreqfiles != 1) { dSFML::Lib::DebugStream::WriteL("Failed to open \"" + path + "\" save file! #C3"); delete sav; mz_zip_reader_end(&zipsave); return (dSFML::Lib::SaveFile*)0; }
    mz_zip_reader_end(&zipsave);
    return sav;
}

dSFML::Lib::SaveFile* dSFML::Lib::SaveFile::SaveFile::GetLastSave(){
    DIR* dir; struct dirent* ent; map<long int, string> m;
    if((dir = opendir("Save")) != NULL){ while((ent = readdir(dir)) != NULL) { if(ent->d_type == DT_REG) { if(dSFML::Lib::has_suffix(string(ent->d_name), ".dssf")){
        struct stat attrib; stat(("Save\\" + string(ent->d_name)).c_str(), &attrib); time_t modtime = attrib.st_mtime;
        m.insert(pair<long int, string>(static_cast<long int>(modtime), "Save\\" + string(ent->d_name)));
    }}} closedir(dir); }
    if(m.empty()) return nullptr;
    auto i = m.end(); i--;
    return dSFML::Lib::SaveFile::LoadSave(i->second);
}

bool dSFML::Lib::SaveFile::RemoveSave(string save){
    if(remove(save.c_str()) != 0)return false;
    else return true;
}
