#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <string>

using namespace std;

namespace dSFML {
    namespace Lib {
        class Exception{
        public:
            enum EType{
                Null = -1,
                Unexcepted = 0,
                ConfigFile_NotFound,
                FileLoad
            };

            Exception();
            Exception(string msg);
            Exception(EType t);
            Exception(string msg, EType t);

            string message;
            EType type;
        };
    }
}

#endif // EXCEPTION_H
