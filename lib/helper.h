#ifndef HELPER_H
#define HELPER_H

#include <SFML/Graphics.hpp>
#include <math.h>
#include <string>
#include <algorithm>
#include <iostream>

using namespace std;

namespace dSFML{
    namespace Lib{
        sf::Image ResizeImage(sf::Image img, int w, int h);
        sf::Image ResizeImageBilinear(sf::Image img, int w2, int h2);
        bool has_suffix(const string& s, const string& suffix);
    }
}

#endif // HELPER_H
