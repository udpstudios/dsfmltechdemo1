#ifndef KEYMAP_H
#define KEYMAP_H

#include <map>
#include <string>
#include <vector>
#include <SFML\Graphics.hpp>
#include "SimpleIni.h"
#include "../config.h"
#include "log.h"

using namespace std;

namespace dSFML{
    namespace Lib{
        class KeyMap{
        private:
            CSimpleIniA ini;
            map<string, sf::Keyboard::Key> kmap = {
                make_pair("TEST_KEY1", sf::Keyboard::Key::Q),
                make_pair("TEST_KEY2", sf::Keyboard::Key::W)
            };
            map<string, sf::Keyboard::Key> kmapBuf;
            map<sf::Keyboard::Key, string> kname = {
                make_pair(sf::Keyboard::Key::Unknown, "Unknown"),
                make_pair(sf::Keyboard::Key::A, "A"),
                make_pair(sf::Keyboard::Key::B, "B"),
                make_pair(sf::Keyboard::Key::C, "C"),
                make_pair(sf::Keyboard::Key::D, "D"),
                make_pair(sf::Keyboard::Key::E, "E"),
                make_pair(sf::Keyboard::Key::F, "F"),
                make_pair(sf::Keyboard::Key::G, "G"),
                make_pair(sf::Keyboard::Key::H, "H"),
                make_pair(sf::Keyboard::Key::I, "I"),
                make_pair(sf::Keyboard::Key::J, "J"),
                make_pair(sf::Keyboard::Key::K, "K"),
                make_pair(sf::Keyboard::Key::L, "L"),
                make_pair(sf::Keyboard::Key::M, "M"),
                make_pair(sf::Keyboard::Key::N, "N"),
                make_pair(sf::Keyboard::Key::O, "O"),
                make_pair(sf::Keyboard::Key::P, "P"),
                make_pair(sf::Keyboard::Key::Q, "Q"),
                make_pair(sf::Keyboard::Key::R, "R"),
                make_pair(sf::Keyboard::Key::S, "S"),
                make_pair(sf::Keyboard::Key::T, "T"),
                make_pair(sf::Keyboard::Key::U, "U"),
                make_pair(sf::Keyboard::Key::V, "V"),
                make_pair(sf::Keyboard::Key::W, "W"),
                make_pair(sf::Keyboard::Key::X, "X"),
                make_pair(sf::Keyboard::Key::Y, "Y"),
                make_pair(sf::Keyboard::Key::Z, "Z"),
                make_pair(sf::Keyboard::Key::Num0, "Num0"),
                make_pair(sf::Keyboard::Key::Num1, "Num1"),
                make_pair(sf::Keyboard::Key::Num2, "Num2"),
                make_pair(sf::Keyboard::Key::Num3, "Num3"),
                make_pair(sf::Keyboard::Key::Num4, "Num4"),
                make_pair(sf::Keyboard::Key::Num5, "Num5"),
                make_pair(sf::Keyboard::Key::Num6, "Num6"),
                make_pair(sf::Keyboard::Key::Num7, "Num7"),
                make_pair(sf::Keyboard::Key::Num8, "Num8"),
                make_pair(sf::Keyboard::Key::Num9, "Num9"),
                make_pair(sf::Keyboard::Key::Escape, "Escape"),
                make_pair(sf::Keyboard::Key::LControl, "LControl"),
                make_pair(sf::Keyboard::Key::LShift, "LShift"),
                make_pair(sf::Keyboard::Key::LAlt, "LAlt"),
                make_pair(sf::Keyboard::Key::LSystem, "LSystem"),
                make_pair(sf::Keyboard::Key::RControl, "RControl"),
                make_pair(sf::Keyboard::Key::RShift, "RShift"),
                make_pair(sf::Keyboard::Key::RAlt, "RAlt"),
                make_pair(sf::Keyboard::Key::RSystem, "RSystem"),
                make_pair(sf::Keyboard::Key::Menu, "Menu"),
                make_pair(sf::Keyboard::Key::LBracket, "LBracket"),
                make_pair(sf::Keyboard::Key::RBracket, "RBracket"),
                make_pair(sf::Keyboard::Key::SemiColon, "SemiColon"),
                make_pair(sf::Keyboard::Key::Comma, "Comma"),
                make_pair(sf::Keyboard::Key::Period, "Period"),
                make_pair(sf::Keyboard::Key::Quote, "Quote"),
                make_pair(sf::Keyboard::Key::Slash, "Slash"),
                make_pair(sf::Keyboard::Key::BackSlash, "BackSlash"),
                make_pair(sf::Keyboard::Key::Tilde, "Tilde"),
                make_pair(sf::Keyboard::Key::Equal, "Equal"),
                make_pair(sf::Keyboard::Key::Dash, "Dash"),
                make_pair(sf::Keyboard::Key::Space, "Space"),
                make_pair(sf::Keyboard::Key::Return, "Return"),
                make_pair(sf::Keyboard::Key::BackSpace, "BackSpace"),
                make_pair(sf::Keyboard::Key::Tab, "Tab"),
                make_pair(sf::Keyboard::Key::PageUp, "PageUp"),
                make_pair(sf::Keyboard::Key::PageDown, "PageDown"),
                make_pair(sf::Keyboard::Key::End, "End"),
                make_pair(sf::Keyboard::Key::Home, "Home"),
                make_pair(sf::Keyboard::Key::Insert, "Insert"),
                make_pair(sf::Keyboard::Key::Delete, "Delete"),
                make_pair(sf::Keyboard::Key::Add, "Add"),
                make_pair(sf::Keyboard::Key::Subtract, "Subtract"),
                make_pair(sf::Keyboard::Key::Multiply, "Multiply"),
                make_pair(sf::Keyboard::Key::Divide, "Divide"),
                make_pair(sf::Keyboard::Key::Left, "Left"),
                make_pair(sf::Keyboard::Key::Right, "Right"),
                make_pair(sf::Keyboard::Key::Up, "Up"),
                make_pair(sf::Keyboard::Key::Down, "Down"),
                make_pair(sf::Keyboard::Key::Numpad0, "Numpad0"),
                make_pair(sf::Keyboard::Key::Numpad1, "Numpad1"),
                make_pair(sf::Keyboard::Key::Numpad2, "Numpad2"),
                make_pair(sf::Keyboard::Key::Numpad3, "Numpad3"),
                make_pair(sf::Keyboard::Key::Numpad4, "Numpad4"),
                make_pair(sf::Keyboard::Key::Numpad5, "Numpad5"),
                make_pair(sf::Keyboard::Key::Numpad6, "Numpad6"),
                make_pair(sf::Keyboard::Key::Numpad7, "Numpad7"),
                make_pair(sf::Keyboard::Key::Numpad8, "Numpad8"),
                make_pair(sf::Keyboard::Key::Numpad9, "Numpad9"),
                make_pair(sf::Keyboard::Key::F1, "F1"),
                make_pair(sf::Keyboard::Key::F2, "F2"),
                make_pair(sf::Keyboard::Key::F3, "F3"),
                make_pair(sf::Keyboard::Key::F4, "F4"),
                make_pair(sf::Keyboard::Key::F5, "F5"),
                make_pair(sf::Keyboard::Key::F6, "F6"),
                make_pair(sf::Keyboard::Key::F7, "F7"),
                make_pair(sf::Keyboard::Key::F8, "F8"),
                make_pair(sf::Keyboard::Key::F9, "F9"),
                make_pair(sf::Keyboard::Key::F10, "F10"),
                make_pair(sf::Keyboard::Key::F11, "F11"),
                make_pair(sf::Keyboard::Key::F12, "F12"),
                make_pair(sf::Keyboard::Key::F13, "LMB"),
                make_pair(sf::Keyboard::Key::F14, "MMB"),
                make_pair(sf::Keyboard::Key::F15, "RMB"),
                make_pair(sf::Keyboard::Key::Pause, "Pause")
            };
            sf::Keyboard::Key Get(map <string, sf::Keyboard::Key> m, string k);
            bool Used(map <string, sf::Keyboard::Key> m, sf::Keyboard::Key key);

        public:
            KeyMap(string str);
            ~KeyMap();
            sf::Keyboard::Key GetKey(string k);
            sf::Keyboard::Key GetKeyBuf(string k);
            sf::Mouse::Button GetMouseKey(string k);
            bool IsPressed(sf::Event e, string k);
            bool IsReleased(sf::Event e, string k);
            bool IsPressed(string k);
            map<string, sf::Keyboard::Key>* GetAllKeys();
            map<string, sf::Keyboard::Key>* GetAllKeysBuf();
            void SetKey(string k, sf::Keyboard::Key key);
            void SetKeyBuf(string k, sf::Keyboard::Key key);
            string KeyToString(sf::Keyboard::Key key);
            sf::Keyboard::Key KeyFromString(string s);
            bool IsUsed(sf::Keyboard::Key key);
            bool IsUsedBuf(sf::Keyboard::Key key);
            void SaveKeyConfig();
            void SaveBuf();
            void RefreshBuf();
        };
    }
}

#endif // KEYMAP_H
