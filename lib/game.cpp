#include "game.h"

/*
Langage code = ISO 639-2!!
*/

dSFML::Lib::Game::Game(vector<string> args){
    isInited = false; isRuning = false; nextStageRun = false;
    forceEnd = false; endGame = false; removeStage = false; stopDrawing = false;
    CSimpleIniA ini; ini.SetUnicode(true); SI_Error rc = ini.LoadFile(CONFIG_FILE_PATH); SI_Error tmp = rc;
    bool WLogDate = ini.GetBoolValue("debug", "logdate", LOG_DATE); ini.SetBoolValue("debug", "logdate", LOG_DATE);
    bool WLogToFile = ini.GetBoolValue("debug", "logtofile", DEBUG_CONSOLE); ini.SetBoolValue("debug", "logtofile", DEBUG_CONSOLE);
    bool WLogToConsole = ini.GetBoolValue("debug", "logtoconsole", DEBUG_FILE); ini.SetBoolValue("debug", "logtoconsole", DEBUG_FILE);
    db = new dSFML::Lib::DebugStream(DEBUGSTREAMLOG_PATH, WLogDate, WLogToConsole, WLogToFile);
    gConsole = new dSFML::Lib::Console(); db->ConnectConsole(gConsole);  sf::err().rdbuf(db); gConsole->Init();
    dSFML::Lib::DebugStream::WriteL("dSFMLTechDemo1 Started.."); //start info
    if(tmp < 0) dSFML::Lib::DebugStream::WriteL("Config file not found. Creating."); //Now we have inited debugstream.
    PLang = new dSFML::Lib::Package("Asserts\\Lang.dsaf", "mul");
    int ww = (int)ini.GetLongValue("display", "width", DEFAULT_WIDTH); if(ww < 800) ww = DEFAULT_WIDTH; ini.SetLongValue("display", "width", (long)ww, NULL);
    int wh = (int)ini.GetLongValue("display", "height", DEFAULT_HEIGHT); if(wh < 640) wh = DEFAULT_HEIGHT; ini.SetLongValue("display", "height", (long)wh, NULL);
    int wb = (int)ini.GetLongValue("display", "bitdepth", DEFAULT_BIT); if(wb != 16 || wb != 32) wb = DEFAULT_BIT; ini.SetLongValue("display", "bitdepth", (long)wb, NULL);
    int aa = (int)ini.GetLongValue("display", "aa", DEFAULT_AALEVEL); if(aa != 0 || aa != 2 || aa != 4) aa = DEFAULT_AALEVEL; ini.SetLongValue("display", "aa", (long)aa, NULL);
    int db = (int)ini.GetLongValue("display", "depthbits", DEFAULT_DEPTHBITS); if(db < 0 || db > 32) db = DEFAULT_DEPTHBITS; ini.SetLongValue("display", "depthbits", (long)db, NULL);
    int sb = (int)ini.GetLongValue("display", "stencilbits", DEFAULT_STENCILBITS); if(sb < 0 || sb > 32) sb = DEFAULT_STENCILBITS; ini.SetLongValue("display", "stencilbits", (long)sb, NULL);
    bool fs = ini.GetBoolValue("display", "fullscreen", DEFAULT_FS); ini.SetBoolValue("display", "fullscreen", fs, NULL);
    bool vs = ini.GetBoolValue("display", "vsync", DEFAULT_VSYNC); ini.SetBoolValue("display", "vsync", vs, NULL);
    showFPS = ini.GetBoolValue("debug", "fpscounter", SHOW_FPSCOUNTER); ini.SetBoolValue("debug", "fpscounter", showFPS, NULL);
    l = ini.GetValue("gameplay", "lang", DEFAULT_LANGUAGE); ini.SetValue("gameplay", "lang", l.c_str()); ls = l;
    ini.SaveFile(CONFIG_FILE_PATH); l += ".ini"; string langini; CSimpleIniA lang; bool loaded = PLang->GetTextFile(l, &langini);
    if(loaded){ lang.SetUnicode(true); rc = lang.LoadData(langini.c_str(), langini.size()); }
    if(!loaded || rc < 0) {
        l = DEFAULT_LANGUAGE; ls = l; l += ".ini"; loaded = PLang->GetTextFile(l, &langini); rc = lang.LoadData(langini.c_str(), langini.size());
        if(!loaded || rc < 0) { dSFML::Lib::DebugStream::WriteL("FATAL ERROR: File [Asserts\\Lang.dsaf]:eng.ini not found!"); throw dSFML::Lib::Exception(dSFML::Lib::Exception::EType::FileLoad); }
    }
    PData = new dSFML::Lib::Package("Asserts\\Data.dsaf", ls); PTextures = new dSFML::Lib::Package("Asserts\\Textures.dsaf", ls);
    dSFML::Lib::DebugStream::WriteL("Loaded: " + l);
    if(fs) w.create(VideoMode(ww, wh, wb), DEFAULT_TITLE, Style::Fullscreen, ContextSettings(db, sb, aa, 2, 0));
    else w.create(VideoMode(ww, wh, wb), DEFAULT_TITLE, Style::Default, ContextSettings(db, sb, aa, 2, 0));
    if(vs) w.setVerticalSyncEnabled(true);
    if(!PTextures->GetFont("fonts/consolefont.ttf", &fps_font)){ dSFML::Lib::DebugStream::WriteL("FATAL ERROR: File [Asserts\\Texturs.dsaf]:fonts/consolefont.ttf not found!"); throw dSFML::Lib::Exception(dSFML::Lib::Exception::EType::FileLoad); }
    fps_text.setFont(fps_font); fps_text.setString("FPS: ?"); fps_text.setCharacterSize(16); fps_text.setColor(Color::Yellow);
    info_text.setFont(fps_font); info_text.setString(string(DEFAULT_TITLE) + " " + string(VESRION)); info_text.setCharacterSize(24); info_text.setColor(Color::Yellow);
    sf::Vector2u wpos = w.getSize(); FloatRect inforect = info_text.getGlobalBounds();
    info_text.setPosition(wpos.x - inforect.width, 0.f);
    k = new KeyMap(KEYMAP_FILE_PATH); gConsole->SetGame(this);
    vector<vector<string>> voa; bool initargs = false;
    for(auto i = args.begin()+1; i != args.end(); i++) {
        string arg = *i;
        if(arg[0] == '+' && arg.size() > 1){
            arg.erase(arg.begin()); voa.push_back(vector<string>());
            auto it = voa.end(); it--; it->push_back(arg); initargs = true;
        }
        else { if(initargs){ auto it = voa.end(); it--; it->push_back(arg); } }
    }
    for(auto i = voa.begin(); i != voa.end(); i++) {
        vector<wstring> vws;
        for(auto j = i->begin(); j != i->end(); j++){ wstring tmpstr(j->begin(), j->end()); vws.push_back(tmpstr); }
        SendCommand(vws);
    }

    //test area!
    /*dSFML::Lib::Package pk("Asserts\\test.dsaf", "pl-pl");
    string str;
    if(pk.GetTextFile("test.txt", &str)) dSFML::Lib::DebugStream::WriteL(str);
    else dSFML::Lib::DebugStream::WriteL("dupa");*/

    dSFMLTechDemo1::sMenu::sMenu* sm = new dSFMLTechDemo1::sMenu::sMenu(); ChangeStage(sm);
}

void dSFML::Lib::Game::ChangeStage(Stage* st){
    if(!isInited) { CurrentStage = st; isInited = true; return; }
    if(isInited) { NextStage = st; nextStageRun = true; Stop(); return; }
}

void dSFML::Lib::Game::Main(){
    while(!endGame){
        if(removeStage) { removeStage = false; delete CurrentStage; }
        if(!isInited) { dSFML::Lib::DebugStream::WriteL("No stage. Waiting to change stage."); continue; }
        if(nextStageRun){ dSFML::Lib::DebugStream::WriteL("Changing stage from \""+CurrentStage->GetName()+"\" to \""+NextStage->GetName()+"\""); delete CurrentStage; CurrentStage = NextStage; nextStageRun = false; }
        if(isInited && !isRuning){
            isRuning = true;
            dSFML::Lib::DebugStream::WriteL("Loading stage \""+CurrentStage->GetName()+"\"");
            CurrentStage->Init(this);
            Thread LoadThread(&Stage::Load_Tick, CurrentStage); LoadThread.launch();
            fps_clock.restart();
            while(!(CurrentStage->IsLoaded() || forceEnd)){
                Event e;
                while(w.pollEvent(e)){
                    if(e.type == sf::Event::Closed) { w.close(); CurrentStage->Load_Tick_End(); LoadThread.wait(); forceEnd = true; endGame = true; continue; }
                    if(e.type == sf::Event::Resized){
                            w.setView(sf::View(sf::FloatRect(0, 0, e.size.width, e.size.height)));
                            sf::Vector2u wpos = w.getSize(); FloatRect inforect = info_text.getGlobalBounds();
                            info_text.setPosition(wpos.x - inforect.width, 0.f);
                    }
                    if(e.type == sf::Event::KeyPressed) {
                        if(e.key.code == sf::Keyboard::Key::Tilde) stopDrawing = !stopDrawing;
                    }
                    CurrentStage->Load_Event(e);
                    if(stopDrawing) gConsole->Events(e);
                }
                w.clear();
                CurrentStage->Load_Draw();
                float fps_thisframetime = fps_clock.restart().asSeconds(); float fps = 1.f / fps_thisframetime;
                if(fps_draw.getElapsedTime().asSeconds() > 1.f) { char fps_buff[255]; sprintf(fps_buff, "FPS: %.1f", fps); string fps_str = fps_buff; fps_text.setString(fps_str); fps_draw.restart(); }
                if(showFPS) w.draw(fps_text);
                if(DRAW_INFO) w.draw(info_text);
                if(stopDrawing) gConsole->Draw(&w);
                w.display();
            }
            CurrentStage->Load_Tick_End(); LoadThread.wait();
            dSFML::Lib::DebugStream::WriteL("Loading stage \""+CurrentStage->GetName()+"\" done. Excecuting stage.");
            Thread PlayThread(&Stage::Play_Tick, CurrentStage); PlayThread.launch();
            fps_clock.restart();
            while(!(CurrentStage->IsClosing() || forceEnd)){
                Event e;
                while(w.pollEvent(e)){
                    if(e.type == sf::Event::Closed) { w.close(); CurrentStage->Play_Tick_End(); PlayThread.wait(); forceEnd = true; endGame = true; continue; }
                    if(e.type == sf::Event::Resized){
                        w.setView(sf::View(sf::FloatRect(0, 0, e.size.width, e.size.height)));
                        sf::Vector2u wpos = w.getSize(); FloatRect inforect = info_text.getGlobalBounds();
                        info_text.setPosition(wpos.x - inforect.width, 0.f);
                    }
                    if(e.type == sf::Event::KeyPressed) {
                        if(e.key.code == sf::Keyboard::Key::Tilde) stopDrawing = !stopDrawing;
                    }
                    CurrentStage->Play_Event(e);
                    if(stopDrawing) gConsole->Events(e);
                }
                w.clear();
                CurrentStage->Play_Draw();
                float fps_thisframetime = fps_clock.restart().asSeconds(); float fps = 1.f / fps_thisframetime;
                if(fps_draw.getElapsedTime().asSeconds() > 1.f) { char fps_buff[255]; sprintf(fps_buff, "FPS: %.1f", fps); string fps_str = fps_buff; fps_text.setString(fps_str); fps_draw.restart(); }
                if(showFPS) w.draw(fps_text);
                if(DRAW_INFO) w.draw(info_text);
                if(stopDrawing) gConsole->Draw(&w);
                w.display();
            }
            CurrentStage->Play_Tick_End(); PlayThread.wait();
            dSFML::Lib::DebugStream::WriteL("Stoping stage \""+CurrentStage->GetName()+"\"");
            dSFML::Lib::DebugStream::WriteL("Destory stage \""+CurrentStage->GetName()+"\" started.");
            CurrentStage->Destroy();
            dSFML::Lib::DebugStream::WriteL("Destory stage \""+CurrentStage->GetName()+"\" ended.");
            isRuning = false; forceEnd = false;
        }
    }
    if(isInited){
        dSFML::Lib::DebugStream::WriteL("dSFMLTechDemo1 Stopping..");
        delete CurrentStage; delete k; delete db; delete gConsole;
        delete PTextures; delete PLang; delete PData;
    } //dtor!!
}

bool dSFML::Lib::Game::SendCommand(vector<wstring> args){
    if(args[0].compare(L"echo") == 0) {
        if(args.size() == 2) dSFML::Lib::DebugStream::WriteWL(args[1]);
        else dSFML::Lib::DebugStream::WriteL("Too few args");
        return true;
    }
    else if(args[0].compare(L"exit") == 0){
        End();
        return true;
    }
    else if(args[0].compare(L"dump") == 0){
        dSFML::Lib::DebugStream::WriteWF(L"Size: %d\n", args.size());
        for(auto i = args.begin(); i != args.end(); i++) dSFML::Lib::DebugStream::WriteWF(L"Arg[%d]: \"%s\"\n", distance(args.begin(), i), (*i).c_str());
        return true;
    }
    else if(args[0].compare(L"bind") == 0) {
        if(args.size() == 1){
            //list of binded keys
            dSFML::Lib::DebugStream::WriteL("List of binded keys: ");
            map<string, sf::Keyboard::Key>* km = k->GetAllKeys();
            for(auto i = km->begin(); i != km->end(); i++) {
                dSFML::Lib::DebugStream::WriteF("%03d|\"%s\"=%s\n", distance(km->begin(), i), i->first.c_str(), k->KeyToString(i->second).c_str());
            }
        }
        else if(args.size() == 3){
            sf::Keyboard::Key key = k->KeyFromString(string(args[2].begin(), args[2].end()));
            k->SetKey(string(args[1].begin(), args[1].end()), key);
        }
        else dSFML::Lib::DebugStream::WriteL("Bad usage of bind");
        return true;
    }
    else if(args[0].compare(L"savekeybind") == 0) {
        k->SaveKeyConfig();
        dSFML::Lib::DebugStream::WriteL("Saved key config!");
    }
    else if(args[0].compare(L"unbind") == 0) {
        if(args.size() == 2){
            string torm = string(args[1].begin(), args[1].end()); map<string, sf::Keyboard::Key>* km = k->GetAllKeys(); auto it = km->find(torm);
            if(it != km->end()){ km->erase(it); dSFML::Lib::DebugStream::WriteL("Unbind ok"); }
            else dSFML::Lib::DebugStream::WriteL("Unbind fail");
        }
        else dSFML::Lib::DebugStream::WriteL("Bad usage of unbind");
    }
    else return false;
}

void dSFML::Lib::Game::Stop(){ if(isInited && isRuning) forceEnd = true; }
void dSFML::Lib::Game::StopAndWait(){ if(isInited && isRuning) forceEnd = true; isInited = false; removeStage = true; }
void dSFML::Lib::Game::End(){ Stop(); endGame = true; }
