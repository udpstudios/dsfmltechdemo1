#ifndef SAVE_H
#define SAVE_H
#define MINIZ_HEADER_FILE_ONLY

#include <vector>
#include <string>
#include <dirent.h>
#include <map>
#include <cstring>
#include <SFML\Graphics.hpp>
#include <sstream>
#include <ctime>
#include <sys/stat.h>
#include <unistd.h>
#include <cstdio>
#include <fstream>
#include "helper.h"
#include "miniz.c"
#include "log.h"
#include "SimpleIni.h"
#include "lang.h"

using namespace std;

namespace dSFML {
    namespace Lib {
        class SaveFile{
        private:
            static dSFML::Lib::SaveFile* LoadSave(string path);
        public:
            //normal
            string FileName;
            wstring SaveName;
            string Quest;
            int Diff;
            int Level;
            sf::Image Pic;
            bool IsPic = false;
            string Path;

            SaveFile(string f);
            ~SaveFile(){}
            //more soon xD Like Handle to zip file

            //static methods
            static vector<dSFML::Lib::SaveFile*> GetSaves();
            static string GetNewSaveName();
            static dSFML::Lib::SaveFile* GetLastSave();
            static bool RemoveSave(string save);
            static dSFML::Lib::SaveFile* CreateNewSave(string f, sf::Image img);
        };
    }
}

#endif // SAVE_H
