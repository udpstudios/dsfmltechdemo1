#ifndef STAGE_H
#define STAGE_H

#include "game.h"
#include <string>
#include <SFML\Graphics.hpp>

using namespace sf;
using namespace std;

namespace dSFML {
    namespace Lib {
        class Game;
        class Stage{
        public:
            virtual ~Stage(){};

            virtual void Init(Game* g) = 0;

            virtual void Load_Tick() = 0;
            virtual void Load_Tick_End() = 0;
            virtual void Load_Draw() = 0;
            virtual void Load_Event(Event e) = 0;

            virtual void Play_Tick() = 0;
            virtual void Play_Tick_End() = 0;
            virtual void Play_Draw() = 0;
            virtual void Play_Event(Event e) = 0;

            virtual void Destroy() = 0;

            virtual bool IsLoaded() = 0;
            virtual bool IsClosing() = 0;
            virtual string GetName() = 0;
        };
    }
}

#endif // STAGE_H

