#include "lang.h"

sf::String dSFML::Lib::GetLangString(CSimpleIniW* ini, string section, string key){
    wstring ret = ini->GetValue(wstring(section.begin(), section.end()).c_str(), wstring(key.begin(), key.end()).c_str(), L"TWOJASTARA");
    if(!ret.compare(L"TWOJASTARA")) ret = wstring(section.begin(), section.end())+L"."+wstring(key.begin(), key.end());
    size_t i = 0; while (true){
        i = ret.find(L"<br>", i); if(i == wstring::npos) break;
        ret.replace(i, 4, L"\n"); i+=4;
    }
    return sf::String(ret);
}

sf::String dSFML::Lib::GetLangFormatedString(CSimpleIniW* ini, string section, string key, ...){
    wstring tmp = GetLangString(ini, section, key).toWideString();
    va_list vlist; va_start(vlist, key); wchar_t* buf = (wchar_t*)malloc(sizeof(wchar_t)*4096);
    vswprintf(buf, tmp.c_str(), vlist);
    va_end(vlist); free(buf); return sf::String(buf);
}

string dSFML::Lib::GetResourcePath(string dir, string res, string lang){
    if(fexists(dir+"\\"+lang+"\\"+res)) return dir+"\\"+lang+"\\"+res;
    if(fexists(dir+"\\"+DEFAULT_LANGUAGE+"\\"+res)) return dir+"\\"+DEFAULT_LANGUAGE+"\\"+res;
    return dir+"\\"+res;
}

bool dSFML::Lib::fexists(const string& filename) {
    ifstream ifile(filename.c_str());
    return ifile;
}
