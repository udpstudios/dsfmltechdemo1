#ifndef VAR_H
#define VAR_H

#include <string>
#include <map>

using namespace std;

namespace dSFML{
    namespace Lib{
        enum VType{
            Null = -1,
            Double = 0,
            Bool,
            String
        };

        class VEntry{
        private:
            VType t;
            void* p;
        public:
            VEntry();
            VType TypeOf();
            double GetDouble();
            bool GetBool();
            string GetString();
            void SetDouble(double a);
            void SetBool(bool a);
            void SetString(string a);
            bool IsEqualDouble(double a);
            bool IsEqualBool(bool a);
            bool IsEqualString(string a);
        };
    }
}

#endif // VAR_H
