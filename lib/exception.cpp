#include "exception.h"

dSFML::Lib::Exception::Exception(){ message = "Default exception"; }
dSFML::Lib::Exception::Exception(string msg) { message = msg; type = EType::Null; }
dSFML::Lib::Exception::Exception(EType t) { type = t; message = "Default exception"; }
dSFML::Lib::Exception::Exception(string msg, EType t){ message = msg; type = t; }
