#ifndef LOG_H
#define LOG_H

#include <string>
#include <ctime>
#include <fstream>
#include <iostream>
#include <streambuf>
#include <sstream>
#include <cstdlib>
#include <cstdarg>
#include <cwchar>
#include "../config.h"
#include "console.h"

using namespace std;

namespace dSFML {
    namespace Lib {
        class DebugStream;
        class Console;
        class DebugStream : public streambuf {
        private:
            wfstream File;
            bool LogDate;
            bool LogToConsole;
            bool LogToFile;
            bool NewLineStarted = true;
            static DebugStream* DebugStr;
            Console* DebugConsole = nullptr;

            void PrintHeader();
            void WriteChar(char c);
            void WriteWChar(wchar_t c);
        public:
            DebugStream(string logpath, bool logdate = true, bool logtostdout = true, bool logtofile = true);
            ~DebugStream();
            virtual int overflow(int c = char_traits<char>::eof());
            void Log(string s);
            void LogLine(string s);
            void LogFormated(string s, ...);
            void LogFormatedV(string s, va_list vlist);
            void WLog(wstring s);
            void WLogLine(wstring s);
            void WLogFormated(wstring s, ...);
            void WLogFormatedV(wstring s, va_list vlist);
            void ConnectConsole(Console* DbCon);
            static void Write(string s);
            static void WriteL(string s);
            static void WriteF(string s, ...);
            static void WriteW(wstring s);
            static void WriteWL(wstring s);
            static void WriteWF(wstring s, ...);
            static DebugStream* GetDebugStr();
        };
    }
}

#endif // LOG_H
