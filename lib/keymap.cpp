#include "keymap.h"

dSFML::Lib::KeyMap::KeyMap(string str){
    //ctor
    ini.SetUnicode(true); SI_Error rc = ini.LoadFile(KEYMAP_FILE_PATH);
    if(rc < 0) dSFML::Lib::DebugStream::WriteL("Keymap file not found. Creating.");
    //for(auto i = kmap.begin(); i != kmap.end(); i++){ int k = (int)ini.GetLongValue("keymap", i->first.c_str(), (long)i->second); ini.SetLongValue("keymap", i->first.c_str(), (long)k); i->second = (sf::Keyboard::Key)k; }
    CSimpleIniA::TNamesDepend ls;
    ini.GetAllKeys("keymap", ls);
    for(auto i = ls.begin(); i != ls.end(); i++) {
        string inik = string(i->pItem); auto it = kmap.find(inik); int k = 0;
        if(it != kmap.end()) { k = (int)ini.GetLongValue("keymap", inik.c_str(), (long)it->second); it->second = (sf::Keyboard::Key)k; }
        else { k = (int)ini.GetLongValue("keymap", inik.c_str(), (long)0); kmap.insert(pair<string, sf::Keyboard::Key>(inik, (sf::Keyboard::Key)k)); }
    }
    ini.SaveFile(KEYMAP_FILE_PATH);
    if(rc<0)dSFML::Lib::DebugStream::WriteL("Keymap file error!");
    kmapBuf.insert(kmap.begin(), kmap.end());
}

dSFML::Lib::KeyMap::~KeyMap(){
    //dtor
    ini.SaveFile(KEYMAP_FILE_PATH);
}

void dSFML::Lib::KeyMap::SetKey(string k, sf::Keyboard::Key key){
    auto it = kmap.find(k);
    if(it != kmap.end()){
        it->second = key;
        ini.SetLongValue("keymap", it->first.c_str(), (long)key);
        ini.SaveFile(KEYMAP_FILE_PATH);
    }
    else {
        kmap[k] = key;
        ini.SetLongValue("keymap", k.c_str(), (long)key);
        ini.SaveFile(KEYMAP_FILE_PATH);
    }
}

void dSFML::Lib::KeyMap::SetKeyBuf(string k, sf::Keyboard::Key key){
    auto it = kmapBuf.find(k);
    if(it != kmapBuf.end()){
        it->second = key;
    }
    else {
        kmapBuf[k] = key;
    }
}
void dSFML::Lib::KeyMap::RefreshBuf() { kmapBuf.clear(); kmapBuf.insert(kmap.begin(), kmap.end()); }
bool dSFML::Lib::KeyMap::Used(map <string, sf::Keyboard::Key> m, sf::Keyboard::Key key){ bool wasUsed = false; for(auto i = m.begin(); i != m.end(); i++){ if(i->second == key && wasUsed) return true; if(i->second == key) wasUsed = true; } return false; }
bool dSFML::Lib::KeyMap::IsUsed(sf::Keyboard::Key key) { return Used(kmap, key); };
bool dSFML::Lib::KeyMap::IsUsedBuf(sf::Keyboard::Key key){ return Used(kmapBuf, key); };
void dSFML::Lib::KeyMap::SaveBuf(){ for(auto i = kmapBuf.begin(); i != kmapBuf.end(); i++) ini.SetLongValue("keymap", i->first.c_str(), (long)i->second); kmap.clear(); kmap.insert(kmapBuf.begin(), kmapBuf.end()); SaveKeyConfig(); }
sf::Keyboard::Key dSFML::Lib::KeyMap::Get(map<string, sf::Keyboard::Key> m, string k){ auto it = m.find(k); if(it != m.end()) return it->second; else return sf::Keyboard::Key::Unknown; }
sf::Keyboard::Key dSFML::Lib::KeyMap::GetKey(string k){ return Get(kmap, k); }
sf::Keyboard::Key dSFML::Lib::KeyMap::GetKeyBuf(string k){ return Get(kmapBuf, k); }

sf::Mouse::Button  dSFML::Lib::KeyMap::GetMouseKey(string k){
    sf::Keyboard::Key ke = GetKey(k);
    if(ke == sf::Keyboard::Key::F13) return sf::Mouse::Button::Left;
    else if(ke == sf::Keyboard::Key::F14) return sf::Mouse::Button::Middle;
    else if(ke == sf::Keyboard::Key::F15) return sf::Mouse::Button::Right;
    else return sf::Mouse::Button::XButton1; //null key xD
}

bool dSFML::Lib::KeyMap::IsPressed(sf::Event e, string k){
    if(e.type == sf::Event::KeyPressed){
        if(e.key.code == GetKey(k)) return true;
        else return false;
    }
    else if(e.type == sf::Event::MouseButtonPressed){
        if(e.mouseButton.button == GetMouseKey(k)) return true;
        else return false;
    }
    else return false;
}

bool dSFML::Lib::KeyMap::IsReleased(sf::Event e, string k){
    if(e.type == sf::Event::KeyReleased){
        if(e.key.code == GetKey(k)) return true;
        else return false;
    }
    else if(e.type == sf::Event::MouseButtonReleased){
        if(e.mouseButton.button == GetMouseKey(k)) return true;
        else return false;
    }
    else return false;
}

bool dSFML::Lib::KeyMap::IsPressed(string k){
    if(sf::Keyboard::isKeyPressed(GetKey(k))) return true;
    else if(sf::Mouse::isButtonPressed(GetMouseKey(k))) return true;
    else return false;
}

map<string, sf::Keyboard::Key>* dSFML::Lib::KeyMap:: GetAllKeys(){ return &kmap; }
map<string, sf::Keyboard::Key>* dSFML::Lib::KeyMap:: GetAllKeysBuf(){ return &kmapBuf; }
string dSFML::Lib::KeyMap::KeyToString(sf::Keyboard::Key key){ auto it = kname.find(key); if(it != kname.end()) return it->second; else return "Unknown"; }
sf::Keyboard::Key dSFML::Lib::KeyMap::KeyFromString(string s){ for(auto i = kname.begin(); i != kname.end(); i++) if(i->second == s) return i->first; return sf::Keyboard::Key::Unknown; }
void dSFML::Lib::KeyMap::SaveKeyConfig(){ SI_Error rc = ini.SaveFile(KEYMAP_FILE_PATH); if(rc<0)dSFML::Lib::DebugStream::WriteL("Keymap file error!"); }
