#ifndef PACKAGEMGR_H
#define PACKAGEMGR_H

#define MINIZ_HEADER_FILE_ONLY

#include <string>
#include <SFML\Graphics.hpp>
#include <cstring>
#include "miniz.c"
#include "exception.h"
#include "lang.h"
#include "log.h"

using namespace std;

namespace dSFML {
    namespace Lib {
        class Package{
            private:
                mz_zip_archive zip;
                string ph;
                string lang;
            public:
                Package(string path, string l);
                ~Package();
                bool GetImage(string path, sf::Image* o);
                bool GetFont(string path, sf::Font* o);
                bool GetTexture(string path, sf::Texture* o);
                bool GetSprite(string path, sf::Sprite* o);
                bool GetTextFile(string path, string* o);
                bool GetWTextFile(string path, wstring* o);
                void* GetFile(string path, size_t* s = 0);
                bool FileExists(string path);
        };
    }
}

#endif // PACKAGEMGR_H
