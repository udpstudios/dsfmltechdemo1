#include "packagemgr.h"

dSFML::Lib::Package::Package(string path, string l){
    if(!dSFML::Lib::fexists(path)) throw dSFML::Lib::Exception(dSFML::Lib::Exception::EType::FileLoad);
    memset(&zip, 0, sizeof(zip)); mz_bool s = mz_zip_reader_init_file(&zip, path.c_str(), 0);
    if(!s) throw dSFML::Lib::Exception(dSFML::Lib::Exception::EType::FileLoad);
    ph =  path; lang = l; dSFML::Lib::DebugStream::WriteL("Package \""+path+"\" is loaded. Language: "+lang);
}

dSFML::Lib::Package::~Package(){
    mz_zip_reader_end(&zip);
}

bool dSFML::Lib::Package::GetImage(string path, sf::Image* o){
    size_t s; void* p = GetFile(path, &s);
    if(p == nullptr) return false;
    if(o->loadFromMemory(p, s)) return true;
    else return false;
}

bool dSFML::Lib::Package::GetFont(string path, sf::Font* o){
    size_t s; void* p = GetFile(path, &s);
    if(p == nullptr) return false;
    if(o->loadFromMemory(p, s)) return true;
    else return false;
}

bool dSFML::Lib::Package::GetTexture(string path, sf::Texture* o){

}

bool dSFML::Lib::Package::GetSprite(string path, sf::Sprite* o){

}

bool dSFML::Lib::Package::GetTextFile(string path, string* o){
    size_t s; char* p = (char*)GetFile(path, &s);
    if(p == nullptr) return false;
    string str(p); *o = str; mz_free(p);
    return true;
}

bool dSFML::Lib::Package::GetWTextFile(string path, wstring* o){
    size_t s; wchar_t* p = (wchar_t*)GetFile(path, &s);
    if(p == nullptr) return false;
    wstring str(p); *o = str; mz_free(p);
    return true;
}

void* dSFML::Lib::Package::GetFile(string path, size_t* s){
    string rpath = lang+"/"+path;
    if(!FileExists(rpath)) rpath = path;
    size_t sz; void* p = mz_zip_reader_extract_file_to_heap(&zip, rpath.c_str(), &sz, 0);
    if(!p) { dSFML::Lib::DebugStream::WriteL("File: ["+ph+"]:"+rpath+" extraction error!"); return nullptr; }
    if(s) *s = sz;
    dSFML::Lib::DebugStream::WriteL("File: ["+ph+"]:"+rpath+" extracted");
    return p;
}

bool dSFML::Lib::Package::FileExists(string path){
    int r = mz_zip_reader_locate_file(&zip, path.c_str(), 0, 0);
    if(r != -1) return true;
    else return false;
}
