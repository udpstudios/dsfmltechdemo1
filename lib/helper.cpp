#include "helper.h"

sf::Image dSFML::Lib::ResizeImage(sf::Image img, int w2, int h2)
{
    if(w2 < -1 || h2 < -1) return img;
    int w1 = (int)img.getSize().x; int h1 = (int)img.getSize().y;
    const int* org = (int*)img.getPixelsPtr();
    if(w2 == -1) w2 = (int)((w1 * h2) / h1);
    if(h2 == -1) h2 = (int)((w2 * h1) / w1);
    int* newimg = new int[w2*h2];
    double xratio = (double)w1/(double)w2;
    double yratio = (double)h1/(double)h2;
    for(int y = 0; y < h2; y++){
        for(int x = 0; x < w2; x++){
            int px = floor(x*xratio);
            int py = floor(y*yratio);
            newimg[(y*w2)+x] = org[(int)((py*w1)+px)];
        }
    }
    sf::Image nimg; nimg.create(w2, h2, (const sf::Uint8*)newimg);
    return nimg;
}

sf::Image dSFML::Lib::ResizeImageBilinear(sf::Image img, int w2, int h2)
{
    if(w2 < -1 || h2 < -1) return img;
    int w1 = (int)img.getSize().x; int h1 = (int)img.getSize().y;
    const int* org = (int*)img.getPixelsPtr();
    if(w2 == -1) w2 = (int)((w1 * h2) / h1);
    if(h2 == -1) h2 = (int)((w2 * h1) / w1);
    int* newimg = new int[w2*h2];
    double xratio = (double)(w1-1)/(double)w2;
    double yratio = (double)(h1-1)/(double)h2;
    double xdiff, ydiff, red, green, blue, alpha;
    int a, b, c, d, x, y, id;
    int offset = 0;
    for(int i = 0; i < h2; i++){
        for(int j = 0; j < w2; j++){
            x = (int)(xratio * j); y = (int)(yratio * i);
            xdiff = (xratio *j) - x; ydiff = (yratio *i) - y;
            id = y*w1+x;
            a = org[id]; b = org[id+1]; c = org[id+w1]; d = org[id+w1+1];
            alpha = ((a>>24)&0xff)*(1-xdiff)*(1-ydiff) + ((b>>24)&0xff)*(xdiff)*(1-ydiff) + ((c>>24)&0xff)*(ydiff)*(1-xdiff) + ((d>>24)&0xff)*(xdiff*ydiff);
            red = ((a>>16)&0xff)*(1-xdiff)*(1-ydiff) + ((b>>16)&0xff)*(xdiff)*(1-ydiff) + ((c>>16)&0xff)*(ydiff)*(1-xdiff) + ((d>>16)&0xff)*(xdiff*ydiff);
            green = ((a>>8)&0xff)*(1-xdiff)*(1-ydiff) + ((b>>8)&0xff)*(xdiff)*(1-ydiff) + ((c>>8)&0xff)*(ydiff)*(1-xdiff) + ((d>>8)&0xff)*(xdiff*ydiff);
            blue = (a&0xff)*(1-xdiff)*(1-ydiff) + (b&0xff)*(xdiff)*(1-ydiff) + (c&0xff)*(ydiff)*(1-xdiff) + (d&0xff)*(xdiff*ydiff);
            newimg[offset++] = ((((int)alpha)<<24)&0xFF000000) | ((((int)red)<<16)&0x00FF0000) | ((((int)green)<<8)&0x0000FF00) | ((int)blue);
        }
    }
    sf::Image nimg; nimg.create(w2, h2, (const sf::Uint8*)newimg);
    return nimg;
}

bool dSFML::Lib::has_suffix(const string& s, const string& suffix)
{
    return (s.size() >= suffix.size()) && equal(suffix.rbegin(), suffix.rend(), s.rbegin());
}


/*
public int[] resizePixels(int[] pixels,int w1,int h1,int w2,int h2) {
    int[] temp = new int[w2*h2] ;
    double x_ratio = w1/(double)w2 ;
    double y_ratio = h1/(double)h2 ;
    double px, py ;
    for (int i=0;i<h2;i++) {
        for (int j=0;j<w2;j++) {
            px = Math.floor(j*x_ratio) ;
            py = Math.floor(i*y_ratio) ;
            temp[(i*w2)+j] = pixels[(int)((py*w1)+px)] ;
        }
    }
    return temp ;
}
*/
