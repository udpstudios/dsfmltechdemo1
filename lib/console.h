#ifndef CONSOLE_H
#define CONSOLE_H

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <vector>
#include <string>
#include <cwchar>
#include "log.h"
#include "exception.h"
#include "game.h"
#include "SimpleIni.h"
#include "lang.h"

using namespace std;

namespace dSFML {
    namespace Lib {
        class Game;
        class Console {
        private:
            vector<wstring*> LineBuf;
            vector<sf::Text*> TextBuf;
            const int bufsize = 256;
            const float interline = 3.5f;
            int pointer = 1;
            int maxp = 0;
            int xoffset = 0;
            int xmax = -500;
            const int xstep = 5;
            float ith = 0.f;
            sf::Font CFont;
            sf::Text OffsetInfo;
            sf::Text IText;
            sf::Vector2u LastSize;
            wstring IStr;
            bool Changed = true;
            sf::Clock clk;
            bool drawsp = true;
            CSimpleIniW ini;
            Game* g;
        public:
            Console();
            ~Console();
            void Init();
            void Events(sf::Event e);
            void Draw(sf::RenderWindow* w);
            void AppendChar(wchar_t c);
            void ProceedCommand(wstring cmd);
            void SetGame(Game* gg);
        };
    }
}

#endif // CONSOLE_H
