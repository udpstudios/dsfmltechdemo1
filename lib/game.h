#ifndef GAME_H
#define GAME_H

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/OpenGL.hpp>
#include <string>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <windows.h>
#include "Stage.h"
#include "log.h"
#include "exception.h"
#include "SimpleIni.h"
#include "keymap.h"
#include "console.h"
#include "packagemgr.h"
#include "../sMenu/sMenu.h"

using namespace sf;
using namespace std;

namespace dSFML{
    namespace Lib{
        class Stage;
        class Console;
        class KeyMap;
        class DebugStream;
        class Package;
        class Game{
        private:
            Font fps_font;
            Text fps_text;
            Text info_text;
            Clock fps_clock;
            Clock fps_draw;
            Stage* CurrentStage;
            Stage* NextStage;
            bool isInited;
            bool isRuning;
            bool nextStageRun;
            bool forceEnd;
            bool endGame;
            bool removeStage;
            bool showFPS;
            Console* gConsole;
            CSimpleIniW lini;
            DebugStream* db;

        public:
            RenderWindow w;
            KeyMap* k;
            Package* PLang;
            Package* PData;
            Package* PTextures;
            string l;
            string ls;
            bool stopDrawing;

            Game(vector<string> args); //Init
            void ChangeStage(Stage* st);
            void Main();
            void Stop();
            void StopAndWait();
            void End();
            bool SendCommand(vector<wstring> args);
        };
    }
}

#endif // GAME_H
