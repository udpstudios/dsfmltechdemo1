#ifndef LANG_H
#define LANG_H

#include <string>
#include <SFML\System.hpp>
#include <fstream>
#include <cstdarg>
#include <cstdlib>
#include <cwchar>
#include "SimpleIni.h"
#include "../config.h"

using namespace std;

namespace dSFML {
    namespace Lib {
        sf::String GetLangString(CSimpleIniW* ini, string section, string key);
        sf::String GetLangFormatedString(CSimpleIniW* ini, string section, string key, ...);
        string GetResourcePath(string dir, string res, string lang);
        bool fexists(const std::string& filename);
    }
}

#endif // LANG_H
