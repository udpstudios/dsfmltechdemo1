#ifndef CONFIG_H
#define CONFIG_H

//DEBUG!!
#define DEBUGON
#define MEMINFODUMP "memdebuginfo.log"

//STATIC CONFIG
#define DEFAULT_TITLE "dSFMLTechDemo1"
#define VESRION "0.0.1a"
#define LOG_FILE_NAME "debugOLD.log"
#define DEBUGSTREAMLOG_PATH "dSFML.log"
#define CONFIG_FILE_PATH "dsfml.ini"
#define KEYMAP_FILE_PATH "keymap.ini"
#define DRAW_INFO true

//DEFAULT CONFIG
#define LOG_DATE false
#define DEBUG_CONSOLE true
#define DEBUG_FILE true
#define SHOW_FPSCOUNTER true
#define DEFAULT_WIDTH 800
#define DEFAULT_HEIGHT 600
#define DEFAULT_BIT 32
#define DEFAULT_FS true
#define DEFAULT_VSYNC false
#define DEFAULT_AALEVEL 0
#define DEFAULT_DEPTHBITS 0
#define DEFAULT_STENCILBITS 0
#define DEFAULT_LANGUAGE "eng"
#define DEFAULT_MV 100
#define DEFAULT_EV 100
#define DEFAULT_MuV 100
#define DEFAULT_VV 100

#endif // CONFIG_H
