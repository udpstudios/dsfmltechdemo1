﻿#include "sMenu.h"

dSFMLTechDemo1::sMenu::sMenu::sMenu(){

}

dSFMLTechDemo1::sMenu::sMenu::~sMenu(){

}

void dSFMLTechDemo1::sMenu::sMenu::Init(Game* gg){
    g = gg;
    g->w.resetGLStates();
    string langini; g->PLang->GetTextFile(g->l, &langini);
    ini.SetUnicode(true); ini.LoadData(langini.c_str(), langini.size());
    /*wchar_t c = L'ą';
    wstring s = L""; s += c;
    dSFML::Lib::DebugStream::WriteWF(L"Entered [char]: %c; Code [int] = %d\n", s[0], (int)s[0]);*/
}

void dSFMLTechDemo1::sMenu::sMenu::Load_Tick(){
    string str; bool ok; bool loaded = g->PData->GetTextFile("menu.style", &str);
    if(loaded) ok = desktop.SetProperties(str);
    if(!ok || !loaded) dSFML::Lib::DebugStream::WriteL("FATAL ERROR: File [Asserts\\Data.dsaf]:menu.style not found!");
    sf::Vector2u wpos = g->w.getSize(); float sp = 0.005f*wpos.x; CurrentSave = dSFML::Lib::SaveFile::GetLastSave();

    //loadinfo
    LoadInfo = sfg::Window::Create(sfg::Window::Style::TITLEBAR | sfg::Window::Style::BACKGROUND); LoadInfo->SetTitle(dSFML::Lib::GetLangString(&ini, "menu", "loadwindowtitle"));
    auto LoadLabel = sfg::Label::Create(); LoadLabel->SetText(dSFML::Lib::GetLangString(&ini, "menu", "loadingtext"));
    auto LoadProgesssBar = sfg::ProgressBar::Create(); LoadProgesssBar->SetRequisition(sf::Vector2f(150.f, 10.f)); LoadProgesssBar->SetFraction(0.5f);
    auto PreLoadBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.f); PreLoadBox->Pack(LoadLabel); PreLoadBox->Pack(LoadProgesssBar); LoadInfo->Add(PreLoadBox); desktop.Add(LoadInfo);
    sf::Vector2f plwsz = LoadInfo->GetRequisition(); LoadInfo->SetPosition(sf::Vector2f(wpos.x/2 - plwsz.x/2, wpos.y/2 - plwsz.y/2));

    //menu window
    MenuWindow = sfg::Window::Create(sfg::Window::Style::BACKGROUND); MenuWindow->Show(false);
    auto MenuBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.f);
    GameLoadButton = sfg::Button::Create(); GameLoadButton->SetClass("BigMenuButton"); MenuBox->Pack(GameLoadButton);
    GameLoadButton->GetSignal(sfg::Widget::OnLeftClick).Connect(bind(&sMenu::OnGameLoadButtonClick, this));
    auto LoadDialogButton = sfg::Button::Create(dSFML::Lib::GetLangString(&ini, "menu", "loaddialogbutton")); LoadDialogButton->SetClass("BigMenuButton"); MenuBox->Pack(LoadDialogButton);
    LoadDialogButton->GetSignal(sfg::Widget::OnLeftClick).Connect(bind(&sMenu::OnLoadDialogButtonClick, this));
    auto COOPButton = sfg::Button::Create(dSFML::Lib::GetLangString(&ini, "menu", "coopbutton")); COOPButton->SetClass("BigMenuButton"); MenuBox->Pack(COOPButton);
    COOPButton->GetSignal(sfg::Widget::OnLeftClick).Connect(bind(&sMenu::OnCOOPButtonClick, this));
    auto ConfigButton = sfg::Button::Create(dSFML::Lib::GetLangString(&ini, "menu", "settingsbutton")); ConfigButton->SetClass("BigMenuButton"); MenuBox->Pack(ConfigButton);
    ConfigButton->GetSignal(sfg::Widget::OnLeftClick).Connect(bind(&sMenu::OnConfigButtonClick, this));
    auto CreditsButton = sfg::Button::Create(dSFML::Lib::GetLangString(&ini, "menu", "creditsbutton")); CreditsButton->SetClass("BigMenuButton"); MenuBox->Pack(CreditsButton);
    CreditsButton->GetSignal(sfg::Widget::OnLeftClick).Connect(bind(&sMenu::OnCreditsButtonClick, this));
    auto QuitButton = sfg::Button::Create(dSFML::Lib::GetLangString(&ini, "menu", "quitbutton")); QuitButton->SetClass("BigMenuButton"); MenuBox->Pack(QuitButton);
    QuitButton->GetSignal(sfg::Widget::OnLeftClick).Connect(bind(&sMenu::OnQuitButtonClick, this));
    MenuWindow->Add(MenuBox); desktop.Add(MenuWindow);
    sf::Vector2f mwsz = MenuWindow->GetRequisition(); MenuWindow->SetPosition(sf::Vector2f(sp, wpos.y - sp - mwsz.y)); //calc position
    if(CurrentSave == nullptr) GameLoadButton->SetLabel(dSFML::Lib::GetLangString(&ini, "menu", "newgamebutton"));
    else GameLoadButton->SetLabel(dSFML::Lib::GetLangString(&ini, "menu", "gameloadbutton"));

    //Loaded save info
    SaveInfoWindow = sfg::Window::Create(sfg::Window::Style::BACKGROUND); SaveInfoWindow->Show(false);
    auto SaveInfoTable = sfg::Table::Create(); SaveInfoTable->SetRequisition(sf::Vector2f(250.f, 0.f));
    //auto SaveInfoFNameLabel = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "load", "fname")); SaveInfoFNameLabel->SetRequisition(sf::Vector2f(75.f, 0.f)); SaveInfoTable->Attach(SaveInfoFNameLabel, sf::Rect<sf::Uint32>(0,0,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    auto SaveInfoNameLabel = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "load", "name")); SaveInfoNameLabel->SetRequisition(sf::Vector2f(75.f, 0.f)); SaveInfoTable->Attach(SaveInfoNameLabel, sf::Rect<sf::Uint32>(0,1,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    auto SaveInfoDiffLabel = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "load", "diff")); SaveInfoDiffLabel->SetRequisition(sf::Vector2f(75.f, 0.f)); SaveInfoTable->Attach(SaveInfoDiffLabel, sf::Rect<sf::Uint32>(0,2,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    auto SaveInfoLvlLabel = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "load", "lvl")); SaveInfoLvlLabel->SetRequisition(sf::Vector2f(75.f, 0.f)); SaveInfoTable->Attach(SaveInfoLvlLabel, sf::Rect<sf::Uint32>(0,3,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    auto SaveInfoQuestLabel = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "load", "quest")); SaveInfoQuestLabel->SetRequisition(sf::Vector2f(75.f, 0.f)); SaveInfoTable->Attach(SaveInfoQuestLabel, sf::Rect<sf::Uint32>(0,4,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    //SaveInfoFNameLabelV = sfg::Label::Create(); SaveInfoFNameLabelV->SetRequisition(sf::Vector2f(75.f, 0.f)); SaveInfoTable->Attach(SaveInfoFNameLabelV, sf::Rect<sf::Uint32>(1,0,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    SaveInfoNameLabelV = sfg::Label::Create(); SaveInfoNameLabelV->SetRequisition(sf::Vector2f(75.f, 0.f)); SaveInfoTable->Attach(SaveInfoNameLabelV, sf::Rect<sf::Uint32>(1,1,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    SaveInfoDiffLabelV = sfg::Label::Create(); SaveInfoDiffLabelV->SetRequisition(sf::Vector2f(75.f, 0.f)); SaveInfoTable->Attach(SaveInfoDiffLabelV, sf::Rect<sf::Uint32>(1,2,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    SaveInfoLvlLabelV = sfg::Label::Create(); SaveInfoLvlLabelV->SetRequisition(sf::Vector2f(75.f, 0.f)); SaveInfoTable->Attach(SaveInfoLvlLabelV, sf::Rect<sf::Uint32>(1,3,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    SaveInfoQuestLabelV = sfg::Label::Create(); SaveInfoQuestLabelV->SetRequisition(sf::Vector2f(75.f, 0.f)); SaveInfoTable->Attach(SaveInfoQuestLabelV, sf::Rect<sf::Uint32>(1,4,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    SaveInfoWindow->Add(SaveInfoTable); desktop.Add(SaveInfoWindow); CurrentSaveRefresh();
    sf::Vector2f lswisz = SaveInfoWindow->GetRequisition(); SaveInfoWindow->SetPosition(sf::Vector2f(wpos.x - sp - lswisz.x, wpos.y - sp - lswisz.y));

    //UDP LOGO XDD
    UDPLogoWindow = sfg::Window::Create(0); UDPLogoWindow->Show(false); sf::Image UDPImg; auto UDPGuiImg = sfg::Image::Create();
    if(!g->PTextures->GetImage("udp.png", &UDPImg)) { dSFML::Lib::DebugStream::WriteL("ERROR: File [Asserts\\Textures.dsaf]:udp.png not found!"); }
    else { UDPImg = dSFML::Lib::ResizeImageBilinear(UDPImg, 250, -1); } UDPLogoWindow->Add(UDPGuiImg); UDPGuiImg->SetImage(UDPImg);
    desktop.Add(UDPLogoWindow);
    sf::Vector2f udpwsz = UDPLogoWindow->GetRequisition(); UDPLogoWindow->SetPosition(sf::Vector2f(wpos.x - sp - udpwsz.x, wpos.y - sp - udpwsz.y - lswisz.y - 10.f));

    //NOOK dialog
    NOOKDialog = sfg::Window::Create(sfg::Window::Style::BACKGROUND | sfg::Window::Style::TITLEBAR); NOOKDialog->SetTitle(dSFML::Lib::GetLangString(&ini, "config", "nookdiagtitle")); NOOKDialog->Show(false);
    auto NOOKBoxV = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.f); auto NOOKBoxH = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL, 5.f);
    auto NOOKLabel = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "config", "nookdiaglabel")); NOOKBoxV->Pack(NOOKLabel);
    auto NOOKDiagOKB = sfg::Button::Create(dSFML::Lib::GetLangString(&ini, "config", "nookdiagokb")); NOOKBoxH->Pack(NOOKDiagOKB);
    NOOKDiagOKB->GetSignal(sfg::Widget::OnLeftClick).Connect(bind(&sMenu::OnOKNOOKDiagClick, this));
    auto NOOKDiagNOOKB = sfg::Button::Create(dSFML::Lib::GetLangString(&ini, "config", "nookdiagnookb")); NOOKBoxH->Pack(NOOKDiagNOOKB);
    NOOKDiagNOOKB->GetSignal(sfg::Widget::OnLeftClick).Connect(bind(&sMenu::OnNOOKNOOKDiagClick, this));
    NOOKBoxV->Pack(NOOKBoxH); NOOKDialog->Add(NOOKBoxV); desktop.Add(NOOKDialog);
    sf::Vector2f nodsz = NOOKDialog->GetRequisition(); NOOKDialog->SetPosition(sf::Vector2f(wpos.x/2 - nodsz.x/2, wpos.y/2 - nodsz.y/2));

    //KeyInfoDialog
    KeyInfoDialog = sfg::Window::Create(sfg::Window::Style::BACKGROUND | sfg::Window::Style::TITLEBAR); KeyInfoDialog->SetTitle(dSFML::Lib::GetLangString(&ini, "config", "keyinfodiagtitle")); KeyInfoDialog->Show(false);
    auto KeyInfoLabel = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "config", "keyinfolabel")); KeyInfoDialog->Add(KeyInfoLabel); desktop.Add(KeyInfoDialog);
    sf::Vector2f kidsz = KeyInfoDialog->GetRequisition(); NOOKDialog->SetPosition(sf::Vector2f(wpos.x/2 - kidsz.x/2, wpos.y/2 - kidsz.y/2));

    //sethings window
    DEVMODE dm = { 0 }; dm.dmSize = sizeof(dm);
    for(int i = 0; EnumDisplaySettings(NULL, i, &dm)!= 0; i++) { if(dm.dmPelsHeight > MaxResH && dm.dmPelsWidth > MaxResW) { MaxResH = dm.dmPelsHeight; MaxResW = dm.dmPelsWidth; } }
    ConfigWindow = sfg::Window::Create(sfg::Window::Style::BACKGROUND | sfg::Window::Style::TITLEBAR); ConfigWindow->SetTitle(dSFML::Lib::GetLangString(&ini, "config", "configwindowtitle")); ConfigWindow->Show(false);
    auto ConfigBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.f);
    NotebookConfig = sfg::Notebook::Create();
    //load config ini
    CSimpleIniA configini; configini.SetUnicode(true); SI_Error rc = configini.LoadFile(CONFIG_FILE_PATH);
    //page1 (video)
    int ww = DEFAULT_WIDTH; int wh = DEFAULT_HEIGHT; int bw = DEFAULT_BIT; int aa = DEFAULT_AALEVEL; int db = DEFAULT_DEPTHBITS; int sb = DEFAULT_STENCILBITS;
    int mvl = DEFAULT_MV; int evl = DEFAULT_EV; int muvl = DEFAULT_MuV; int vvl = DEFAULT_VV;
    string langl = DEFAULT_LANGUAGE;
    bool resselected = false;
    if(rc > -1) {
            ww = (int)configini.GetLongValue("display", "width", DEFAULT_WIDTH); if(ww < 800) ww = DEFAULT_WIDTH;
            wh = (int)configini.GetLongValue("display", "height", DEFAULT_HEIGHT); if(wh < 640) wh = DEFAULT_HEIGHT;
            bw = (int)configini.GetLongValue("display", "bitdepth", DEFAULT_BIT); if(bw != 16 || bw != 32) bw = DEFAULT_BIT;
            aa = (int)configini.GetLongValue("display", "aa", DEFAULT_AALEVEL); if(aa != 0 || aa != 2 || aa != 4) aa = DEFAULT_AALEVEL;
            db = (int)configini.GetLongValue("display", "depthbits", DEFAULT_DEPTHBITS); if(db < 0 || db > 32) db = DEFAULT_DEPTHBITS;
            sb = (int)configini.GetLongValue("display", "stencilbits", DEFAULT_STENCILBITS); if(sb < 0 || sb > 32) sb = DEFAULT_STENCILBITS;
            mvl = (int)configini.GetLongValue("audio", "sound", DEFAULT_MV); if(mvl < 0 || mvl > 100) mvl = DEFAULT_MV;
            evl = (int)configini.GetLongValue("audio", "effect", DEFAULT_EV); if(evl < 0 || evl > 100) evl = DEFAULT_EV;
            muvl = (int)configini.GetLongValue("audio", "music", DEFAULT_MuV); if(muvl < 0 || muvl > 100) muvl = DEFAULT_MuV;
            langl = configini.GetValue("gameplay", "lang", DEFAULT_LANGUAGE);
    }
    auto VideoPageTable = sfg::Table::Create(); VideoPageTable->SetRequisition(sf::Vector2f(400.f, 300.f));
    //resolution combobox
    auto ResolutionLabel = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "config", "resolution")); VideoPageTable->Attach(ResolutionLabel, sf::Rect<sf::Uint32>(0,0,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    ResolutionCB = sfg::ComboBox::Create(); for(auto i = Resolutions.begin(); i < Resolutions.end(); i++) {
        if((*i)[0] > MaxResW || (*i)[1] > MaxResH) continue;
        char buf[64]; sprintf(buf, "%dx%dx%d", (*i)[0], (*i)[1], (*i)[2]); ResolutionCB->AppendItem(buf);
        if(ww == (*i)[0] && wh == (*i)[1] && bw == (*i)[2]) { ResolutionCB->SelectItem(distance(Resolutions.begin(), i)); resselected = true;
    }}
    if(!resselected) ResolutionCB->SelectItem(0); resselected = false; VideoPageTable->Attach(ResolutionCB, sf::Rect<sf::Uint32>(1,0,1,1), sfg::Table::FILL | sfg::Table::EXPAND, 0, sf::Vector2f(5.f, 5.f));
    //fullscr
    auto FullscreenLabel = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "config", "fullscreen")); VideoPageTable->Attach(FullscreenLabel, sf::Rect<sf::Uint32>(0,1,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    FullscreenChkBox = sfg::CheckButton::Create(""); bool fs = configini.GetBoolValue("display", "fullscreen", DEFAULT_FS); FullscreenChkBox->SetActive(fs); VideoPageTable->Attach(FullscreenChkBox, sf::Rect<sf::Uint32>(1,1,1,1), sfg::Table::FILL | sfg::Table::EXPAND, 0, sf::Vector2f(5.f, 5.f));
    //vsync
    auto VSyncLabel = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "config", "vsync")); VideoPageTable->Attach(VSyncLabel, sf::Rect<sf::Uint32>(0,2,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    VSyncChkBox = sfg::CheckButton::Create(""); bool vs = configini.GetBoolValue("display", "vsync", DEFAULT_VSYNC); VSyncChkBox->SetActive(vs); VideoPageTable->Attach(VSyncChkBox, sf::Rect<sf::Uint32>(1,2,1,1), sfg::Table::FILL | sfg::Table::EXPAND, 0, sf::Vector2f(5.f, 5.f));
    //aa (set for 0;2;4;8)
    auto AALabel = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "config", "aa")); VideoPageTable->Attach(AALabel, sf::Rect<sf::Uint32>(0,3,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    AACB = sfg::ComboBox::Create(); for(auto i = AALevels.begin(); i < AALevels.end(); i++) { char buf[64]; sprintf(buf, "%dx", *i); AACB->AppendItem(buf); if(aa == *i) { AACB->SelectItem(distance(AALevels.begin(), i)); resselected = true; }}
    if(!resselected) AACB->SelectItem(0); resselected = false; VideoPageTable->Attach(AACB, sf::Rect<sf::Uint32>(1,3,1,1), sfg::Table::FILL | sfg::Table::EXPAND, 0, sf::Vector2f(5.f, 5.f));
    //depthbits <0; 24> step 2
    auto DBLabel = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "config", "db")); VideoPageTable->Attach(DBLabel, sf::Rect<sf::Uint32>(0,4,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    DBCB = sfg::ComboBox::Create(); for(auto i = DBLevels.begin(); i < DBLevels.end(); i++) { char buf[64]; sprintf(buf, "%d", *i); DBCB->AppendItem(buf); if(db == *i) { DBCB->SelectItem(distance(DBLevels.begin(), i)); resselected = true; }} if(!resselected) DBCB->SelectItem(0); resselected = false; VideoPageTable->Attach(DBCB, sf::Rect<sf::Uint32>(1,4,1,1), sfg::Table::FILL | sfg::Table::EXPAND, 0, sf::Vector2f(5.f, 5.f));
    //stencilbits 0;2;4;8
    auto SBLabel = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "config", "sb")); VideoPageTable->Attach(SBLabel, sf::Rect<sf::Uint32>(0,5,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    SBCB = sfg::ComboBox::Create(); for(auto i = SBLevels.begin(); i < SBLevels.end(); i++) { char buf[64]; sprintf(buf, "%d", *i); SBCB->AppendItem(buf); if(sb == *i) { SBCB->SelectItem(distance(SBLevels.begin(), i)); resselected = true; }}
    if(!resselected) SBCB->SelectItem(0); resselected = false; VideoPageTable->Attach(SBCB, sf::Rect<sf::Uint32>(1,5,1,1), sfg::Table::FILL | sfg::Table::EXPAND, 0, sf::Vector2f(5.f, 5.f));    //pageadding
    auto InfoVideoLabel = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "config", "videoinfo")); VideoPageTable->Attach(InfoVideoLabel, sf::Rect<sf::Uint32>(0,6,2,1), 0, 0, sf::Vector2f(5.f, 5.f));

    //page 2 audio
    auto AudioPageTable = sfg::Table::Create(); AudioPageTable->SetRequisition(sf::Vector2f(400.f, 300.f));
    //Master volume
    auto MasterVolLab = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "config", "mastervollab")); AudioPageTable->Attach(MasterVolLab, sf::Rect<sf::Uint32>(0,0,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    MVS = sfg::Scale::Create(0.f, 110.f, 1.f, sfg::Scale::Orientation::HORIZONTAL); MVS->GetAdjustment()->SetPageSize(10.f); MVS->SetRequisition(sf::Vector2f(0.f, 20.f)); AudioPageTable->Attach(MVS, sf::Rect<sf::Uint32>(1,0,1,1), sfg::Table::FILL | sfg::Table::EXPAND, 0, sf::Vector2f(5.f, 5.f));
    MVS->GetAdjustment()->GetSignal(sfg::Adjustment::OnChange).Connect(bind(&sMenu::MVSi, this));
    MVSLabel = sfg::Label::Create("0"); MVSLabel->SetRequisition(sf::Vector2f(45.f, 0.f)); AudioPageTable->Attach(MVSLabel, sf::Rect<sf::Uint32>(2,0,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    MVS->GetAdjustment()->SetValue(mvl);
    //Effects volume
    auto EffectsVolLab = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "config", "effectsvollab")); AudioPageTable->Attach(EffectsVolLab, sf::Rect<sf::Uint32>(0,1,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    EVS = sfg::Scale::Create(0.f, 110.f, 1.f, sfg::Scale::Orientation::HORIZONTAL); EVS->GetAdjustment()->SetPageSize(10.f); EVS->SetRequisition(sf::Vector2f(0.f, 20.f)); AudioPageTable->Attach(EVS, sf::Rect<sf::Uint32>(1,1,1,1), sfg::Table::FILL | sfg::Table::EXPAND, 0, sf::Vector2f(5.f, 5.f));
    EVS->GetAdjustment()->GetSignal(sfg::Adjustment::OnChange).Connect(bind(&sMenu::EVSi, this));
    EVSLabel = sfg::Label::Create("0"); EVSLabel->SetRequisition(sf::Vector2f(45.f, 0.f)); AudioPageTable->Attach(EVSLabel, sf::Rect<sf::Uint32>(2,1,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    EVS->GetAdjustment()->SetValue((float)evl);
    //Music volume
    auto MusicVolLab = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "config", "musicvollab")); AudioPageTable->Attach(MusicVolLab, sf::Rect<sf::Uint32>(0,2,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    MuVS = sfg::Scale::Create(0.f, 110.f, 1.f, sfg::Scale::Orientation::HORIZONTAL); MuVS->GetAdjustment()->SetPageSize(10.f); MuVS->SetRequisition(sf::Vector2f(0.f, 20.f)); AudioPageTable->Attach(MuVS, sf::Rect<sf::Uint32>(1,2,1,1), sfg::Table::FILL | sfg::Table::EXPAND, 0, sf::Vector2f(5.f, 5.f));
    MuVS->GetAdjustment()->GetSignal(sfg::Adjustment::OnChange).Connect(bind(&sMenu::MuVSi, this));
    MuVSLabel = sfg::Label::Create("0"); MuVSLabel->SetRequisition(sf::Vector2f(45.f, 0.f)); AudioPageTable->Attach(MuVSLabel, sf::Rect<sf::Uint32>(2,2,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    MuVS->GetAdjustment()->SetValue((float)muvl);
    //Voice volume
    auto VoiceVolLab = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "config", "voicevollab")); AudioPageTable->Attach(VoiceVolLab, sf::Rect<sf::Uint32>(0,3,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    VVS = sfg::Scale::Create(0.f, 110.f, 1.f, sfg::Scale::Orientation::HORIZONTAL); VVS->GetAdjustment()->SetPageSize(10.f); VVS->SetRequisition(sf::Vector2f(0.f, 20.f)); AudioPageTable->Attach(VVS, sf::Rect<sf::Uint32>(1,3,1,1), sfg::Table::FILL | sfg::Table::EXPAND, 0, sf::Vector2f(5.f, 5.f));
    VVS->GetAdjustment()->GetSignal(sfg::Adjustment::OnChange).Connect(bind(&sMenu::VVSi, this));
    VVSLabel = sfg::Label::Create("0"); VVSLabel->SetRequisition(sf::Vector2f(45.f, 0.f)); AudioPageTable->Attach(VVSLabel, sf::Rect<sf::Uint32>(2,3,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    VVS->GetAdjustment()->SetValue((float)vvl);

    //page 3 gameplay
    auto GPPageTable = sfg::Table::Create(); GPPageTable->SetRequisition(sf::Vector2f(400.f, 300.f));
    auto LangLabel = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "config", "langcb")); GPPageTable->Attach(LangLabel, sf::Rect<sf::Uint32>(0,0,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    LangCB = sfg::ComboBox::Create(); for(auto i = Lang.begin(); i < Lang.end(); i++) { LangCB->AppendItem(dSFML::Lib::GetLangString(&ini, "lang", *i)); if(langl == *i) { LangCB->SelectItem(distance(Lang.begin(), i)); resselected = true; }} if(!resselected) DBCB->SelectItem(0); resselected = false; GPPageTable->Attach(LangCB, sf::Rect<sf::Uint32>(1,0,1,1), sfg::Table::FILL | sfg::Table::EXPAND, 0, sf::Vector2f(5.f, 5.f));
    auto InfoGameLabel = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "config", "gameinfo")); GPPageTable->Attach(InfoGameLabel, sf::Rect<sf::Uint32>(0,1,2,1), 0, 0, sf::Vector2f(5.f, 5.f));

    //page 4 controls
    auto ControlsPageTable = sfg::Table::Create(); ControlsPageTable->SetRequisition(sf::Vector2f(400.f, 300.f));
    auto EmptyLabelC = sfg::Label::Create(); EmptyLabelC->SetClass("ELabelC");
    ControlsPageTable->Attach(EmptyLabelC, sf::Rect<sf::Uint32>(0,0,2,1), sfg::Table::FILL | sfg::Table::EXPAND, 0, sf::Vector2f(5.f, 5.f));
    for(auto i = g->k->GetAllKeys()->begin(); i != g->k->GetAllKeys()->end(); i++){
        //auto EmptyLabelC2 = sfg::Label::Create(); EmptyLabelC2->SetClass("ELabelC");  ControlsPageTable->Attach(EmptyLabelC2, sf::Rect<sf::Uint32>(0,distance(g->k->GetAllKeys()->begin(), i)+1,2,1), 0, 0, sf::Vector2f(5.f, 5.f));
        auto InfoLabel = sfg::Label::Create("  "+dSFML::Lib::GetLangString(&ini, "keys", i->first)); ControlsPageTable->Attach(InfoLabel, sf::Rect<sf::Uint32>(0,distance(g->k->GetAllKeys()->begin(), i)+1,1,1), 0, 0, sf::Vector2f(3.f, 3.f));
        auto KeyLabel = sfg::Button::Create(dSFML::Lib::GetLangString(&ini, "keymap", g->k->KeyToString(i->second))); ControlsPageTable->Attach(KeyLabel, sf::Rect<sf::Uint32>(1,distance(g->k->GetAllKeys()->begin(), i)+1,1,1), sfg::Table::FILL | sfg::Table::EXPAND, 0, sf::Vector2f(3.f, 3.f));
        InfoLabel->SetRequisition(sf::Vector2f(200.f, 0.f)); KeyLabel->SetRequisition(sf::Vector2f(150.f, 0.f));
        auto KeyC = new ControlLabelC(i->first, this, KeyLabel); KeyLabel->GetSignal(sfg::Widget::OnLeftClick).Connect(bind(&ControlLabelC::OnSignal, KeyC));
        ControlsButtonLabels.push_back(KeyLabel); ControlLabelsClasses.push_back(KeyC); if(g->k->IsUsed(i->second)) KeyLabel->SetClass("Used");
    }
    auto ControlsScroll = sfg::ScrolledWindow::Create(); ControlsScroll->SetScrollbarPolicy(sfg::ScrolledWindow::VERTICAL_ALWAYS | sfg::ScrolledWindow::HORIZONTAL_NEVER);
    ControlsScroll->AddWithViewport(ControlsPageTable); ControlsScroll->SetRequisition(sf::Vector2f(300.f, 200.f));

    //Noteconfig
    NotebookConfig->AppendPage(VideoPageTable, sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "config", "videopage")));
    NotebookConfig->AppendPage(AudioPageTable, sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "config", "audiopage")));
    NotebookConfig->AppendPage(GPPageTable, sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "config", "gppage")));
    NotebookConfig->AppendPage(ControlsScroll, sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "config", "controlspage")));
    NotebookConfig->SetScrollable(true); NotebookConfig->SetRequisition(sf::Vector2f(400.f, 300.f)); ConfigBox->Pack(NotebookConfig);
    auto ConfigBoxH = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL, 5.f);
    auto OKButton = sfg::Button::Create(dSFML::Lib::GetLangString(&ini, "config", "okbutton")); ConfigBoxH->Pack(OKButton);
    auto NOOKButton = sfg::Button::Create(dSFML::Lib::GetLangString(&ini, "config", "nookbutton")); ConfigBoxH->Pack(NOOKButton);
    auto ApplyButton = sfg::Button::Create(dSFML::Lib::GetLangString(&ini, "config", "applybutton")); ConfigBoxH->Pack(ApplyButton);
    OKButton->GetSignal(sfg::Widget::OnLeftClick ).Connect(bind(&sMenu::OnOKButtonClick, this));
    NOOKButton->GetSignal(sfg::Widget::OnLeftClick ).Connect(bind(&sMenu::OnNOOKButtonClick, this));
    ApplyButton->GetSignal(sfg::Widget::OnLeftClick ).Connect(bind(&sMenu::OnApplyButtonClick, this));
    ConfigBox->Pack(ConfigBoxH); ConfigWindow->Add(ConfigBox); desktop.Add(ConfigWindow);
    sf::Vector2f cwsz = ConfigWindow->GetRequisition(); ConfigWindow->SetPosition(sf::Vector2f(wpos.x/2 - cwsz.x/2, wpos.y/2 - cwsz.y/2));

    //LoadWindow
    LoadWindow = sfg::Window::Create(sfg::Window::Style::BACKGROUND | sfg::Window::Style::TITLEBAR); LoadWindow->SetTitle(dSFML::Lib::GetLangString(&ini, "load", "loadwindowtitle")); LoadWindow->Show(false);
    auto LoadBox = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL, 5.f);
    LoadTable = sfg::Table::Create(); LoadTable->SetRequisition(sf::Vector2f(300.f, 0.f));
    LoadWindow->Add(LoadBox); LoadWindow->SetRequisition(sf::Vector2f(500.f, 400.f)); desktop.Add(LoadWindow);
    auto LoadScroll = sfg::ScrolledWindow::Create(); LoadScroll->SetScrollbarPolicy(sfg::ScrolledWindow::VERTICAL_ALWAYS | sfg::ScrolledWindow::HORIZONTAL_NEVER);
    LoadScroll->AddWithViewport(LoadTable); LoadScroll->SetRequisition(sf::Vector2f(300.f, 400.f)); LoadBox->Pack(LoadScroll);
    auto LoadBoxSide = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.f); LoadBoxSide->SetRequisition(sf::Vector2f(250.f, 0.f));
    LoadImg = sfg::Image::Create();

    if(!g->PTextures->GetImage("savepic-notfound.png", &LImg)) { dSFML::Lib::DebugStream::WriteL("ERROR: File [Asserts\\Textures.dsaf]:savepic-notfound.png not found!"); }
    else { LImg = dSFML::Lib::ResizeImageBilinear(LImg, 240, -1); } LoadBoxSide->Pack(LoadImg, false, false);

    auto LoadInfoTable = sfg::Table::Create(); LoadInfoTable->SetRequisition(sf::Vector2f(250.f, 0.f));
    auto LoadNameLabel = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "load", "name")); LoadNameLabel->SetRequisition(sf::Vector2f(75.f, 0.f)); LoadInfoTable->Attach(LoadNameLabel, sf::Rect<sf::Uint32>(0,0,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    auto LoadDiffLabel = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "load", "diff")); LoadDiffLabel->SetRequisition(sf::Vector2f(75.f, 0.f)); LoadInfoTable->Attach(LoadDiffLabel, sf::Rect<sf::Uint32>(0,1,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    auto LoadLvlLabel = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "load", "lvl")); LoadLvlLabel->SetRequisition(sf::Vector2f(75.f, 0.f)); LoadInfoTable->Attach(LoadLvlLabel, sf::Rect<sf::Uint32>(0,2,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    auto LoadQuestLabel = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "load", "quest")); LoadQuestLabel->SetRequisition(sf::Vector2f(75.f, 0.f)); LoadInfoTable->Attach(LoadQuestLabel, sf::Rect<sf::Uint32>(0,3,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    LoadNameLabelV = sfg::Label::Create(); LoadNameLabelV->SetRequisition(sf::Vector2f(75.f, 0.f)); LoadInfoTable->Attach(LoadNameLabelV, sf::Rect<sf::Uint32>(1,0,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    LoadDiffLabelV = sfg::Label::Create(); LoadDiffLabelV->SetRequisition(sf::Vector2f(75.f, 0.f)); LoadInfoTable->Attach(LoadDiffLabelV, sf::Rect<sf::Uint32>(1,1,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    LoadLvlLabelV = sfg::Label::Create(); LoadLvlLabelV->SetRequisition(sf::Vector2f(75.f, 0.f)); LoadInfoTable->Attach(LoadLvlLabelV, sf::Rect<sf::Uint32>(1,2,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    LoadQuestLabelV = sfg::Label::Create(); LoadQuestLabelV->SetRequisition(sf::Vector2f(75.f, 0.f)); LoadInfoTable->Attach(LoadQuestLabelV, sf::Rect<sf::Uint32>(1,3,1,1), 0, 0, sf::Vector2f(5.f, 5.f));
    LoadBoxSide->Pack(LoadInfoTable, false, false);
    auto LoadButton = sfg::Button::Create(dSFML::Lib::GetLangString(&ini, "load", "loadload")); LoadBoxSide->Pack(LoadButton, false, false);
    auto LoadDelButton = sfg::Button::Create(dSFML::Lib::GetLangString(&ini, "load", "loaddel")); LoadBoxSide->Pack(LoadDelButton, false, false);
    auto LoadCloseButton = sfg::Button::Create(dSFML::Lib::GetLangString(&ini, "load", "loadclose")); LoadBoxSide->Pack(LoadCloseButton, false, false);
    LoadButton->GetSignal(sfg::Widget::OnLeftClick ).Connect(bind(&sMenu::OnLoadLoadButton, this));
    LoadDelButton->GetSignal(sfg::Widget::OnLeftClick ).Connect(bind(&sMenu::OnLoadDeleteButton, this));
    LoadCloseButton->GetSignal(sfg::Widget::OnLeftClick ).Connect(bind(&sMenu::OnLoadExitButton, this));
    LoadBox->Pack(LoadBoxSide);
    sf::Vector2f lwsz = LoadWindow->GetRequisition(); LoadWindow->SetPosition(sf::Vector2f(wpos.x/2 - lwsz.x/2, wpos.y/2 - lwsz.y/2));

    //DeleteAskDialog
    DeleteAskDialog = sfg::Window::Create(sfg::Window::Style::BACKGROUND | sfg::Window::Style::TITLEBAR); DeleteAskDialog->SetTitle(dSFML::Lib::GetLangString(&ini, "savediag", "saveaskdiagtitle")); DeleteAskDialog->Show(false);
    auto DeleteAskBoxV = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.f); auto DeleteAskBoxH = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL, 5.f);
    auto DeleteAskLabel = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "savediag", "deldiaglabel")); DeleteAskBoxV->Pack(DeleteAskLabel);
    auto DeleteAskDiagOKB = sfg::Button::Create(dSFML::Lib::GetLangString(&ini, "savediag", "deldiaglabelokb")); DeleteAskBoxH->Pack(DeleteAskDiagOKB);
    DeleteAskDiagOKB->GetSignal(sfg::Widget::OnLeftClick).Connect(bind(&sMenu::OnOKDeleteAskDiagClick, this));
    auto DeleteAskDiagNOB = sfg::Button::Create(dSFML::Lib::GetLangString(&ini, "savediag", "deldiaglabelnookb")); DeleteAskBoxH->Pack(DeleteAskDiagNOB);
    DeleteAskDiagNOB->GetSignal(sfg::Widget::OnLeftClick).Connect(bind(&sMenu::OnNODeleteAskDiagClick, this));
    DeleteAskBoxV->Pack(DeleteAskBoxH); DeleteAskDialog->Add(DeleteAskBoxV); desktop.Add(DeleteAskDialog);
    sf::Vector2f dadsz = DeleteAskDialog->GetRequisition(); DeleteAskDialog->SetPosition(sf::Vector2f(wpos.x/2 - dadsz.x/2, wpos.y/2 - dadsz.y/2));

    //DeleteFailDialog
    DeleteFailDialog = sfg::Window::Create(sfg::Window::Style::BACKGROUND | sfg::Window::Style::TITLEBAR); DeleteFailDialog->SetTitle(dSFML::Lib::GetLangString(&ini, "savediag", "saveaskdiagfailtitle")); DeleteFailDialog->Show(false);
    auto DeleteFailBoxV = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.f); auto DeleteFailBoxH = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL, 5.f);
    auto DeleteFailLabel = sfg::Label::Create(dSFML::Lib::GetLangString(&ini, "savediag", "saveaskfaillabel")); DeleteFailBoxV->Pack(DeleteFailLabel);
    auto DeleteFailDiagOKB = sfg::Button::Create(dSFML::Lib::GetLangString(&ini, "savediag", "saveaskfailokb")); DeleteFailBoxH->Pack(DeleteFailDiagOKB);
    DeleteFailDiagOKB->GetSignal(sfg::Widget::OnLeftClick).Connect(bind(&sMenu::ONOKDeleteFailDiagClick, this));
    DeleteFailBoxV->Pack(DeleteFailBoxH); DeleteFailDialog->Add(DeleteFailBoxV); desktop.Add(DeleteFailDialog);
    sf::Vector2f dadfsz = DeleteFailDialog->GetRequisition(); DeleteFailDialog->SetPosition(sf::Vector2f(wpos.x/2 - dadfsz.x/2, wpos.y/2 - dadfsz.y/2));

    //for(int i = 0; i < 20; i++) dSFML::Lib::DebugStream::WriteF("Test debug message #%d\n", i);

    //sf::sleep(sf::seconds(5));
    Loaded = true;
    while(!EndTick) while(g->stopDrawing) if(EndTick) break;
    EndTick = false;
}

void dSFMLTechDemo1::sMenu::sMenu::Load_Draw(){
    desktop.Update(clock.restart().asSeconds());
    sfgui.Display(g->w);
}

void dSFMLTechDemo1::sMenu::sMenu::Load_Event(Event e){
    if(!g->stopDrawing){
        desktop.HandleEvent(e);
    }
    if(e.type == sf::Event::Resized){
        sf::Vector2u wpos = g->w.getSize(); sf::Vector2f lwsz = LoadInfo->GetRequisition(); LoadInfo->SetPosition(sf::Vector2f(wpos.x/2 - lwsz.x/2, wpos.y/2 - lwsz.y/2));
    }
}

void dSFMLTechDemo1::sMenu::sMenu::Play_Tick(){
    LoadInfo->Show(false); MenuWindow->Show(true); SaveInfoWindow->Show(true); UDPLogoWindow->Show(true);
    desktop.Refresh();
    while(!EndTick) while(g->stopDrawing) if(EndTick) break;
    EndTick = false;
}

void dSFMLTechDemo1::sMenu::sMenu::Play_Draw(){
    desktop.Update(clock.restart().asSeconds());
    sfgui.Display(g->w);
}

void dSFMLTechDemo1::sMenu::sMenu::Play_Event(Event e){
    if(!g->stopDrawing){
        desktop.HandleEvent(e);
        if(!KeyWait){
            //if(g->k->IsPressed(e, "TEST_KEY2")) dSFML::Lib::DebugStream::WriteL("You clicked \"TEST_KEY2\" using smth");
        }

        if(e.type == sf::Event::KeyPressed){
            if(KeyWait){ g->k->SetKeyBuf(KeyToChange, e.key.code); KeyInfoDialog->Show(false); ControlsPageTableRefresh(); ConfigWindow->SetState(sfg::Widget::State::NORMAL); KeyWait = false; }
        }
        if(e.type == sf::Event::MouseButtonPressed){
            if(KeyWait){
                bool ok = false;
                if(e.mouseButton.button == sf::Mouse::Button::Left) { g->k->SetKeyBuf(KeyToChange, sf::Keyboard::Key::F13); ok = !ok; }
                if(e.mouseButton.button == sf::Mouse::Button::Middle) { g->k->SetKeyBuf(KeyToChange, sf::Keyboard::Key::F14); ok = !ok; }
                if(e.mouseButton.button == sf::Mouse::Button::Right) { g->k->SetKeyBuf(KeyToChange, sf::Keyboard::Key::F15); ok = !ok; }
                if(ok) KeyInfoDialog->Show(false); ControlsPageTableRefresh(); ConfigWindow->SetState(sfg::Widget::State::NORMAL); KeyWait = false;
            }
        }
    }
    if(e.type == sf::Event::Resized){
        sf::Vector2u wpos = g->w.getSize(); float sp = 0.005f*wpos.x;
        sf::Vector2f mwsz = MenuWindow->GetRequisition(); MenuWindow->SetPosition(sf::Vector2f(sp, wpos.y - sp - mwsz.y));
        sf::Vector2f cwsz = ConfigWindow->GetRequisition(); ConfigWindow->SetPosition(sf::Vector2f(wpos.x/2 - cwsz.x/2, wpos.y/2 - cwsz.y/2));
        sf::Vector2f nodsz = NOOKDialog->GetRequisition(); NOOKDialog->SetPosition(sf::Vector2f(wpos.x/2 - nodsz.x/2, wpos.y/2 - nodsz.y/2));
        sf::Vector2f kidsz = KeyInfoDialog->GetRequisition(); KeyInfoDialog->SetPosition(sf::Vector2f(wpos.x/2 - kidsz.x/2, wpos.y/2 - kidsz.y/2));
        sf::Vector2f lwsz = LoadWindow->GetRequisition(); LoadWindow->SetPosition(sf::Vector2f(wpos.x/2 - lwsz.x/2, wpos.y/2 - lwsz.y/2));
        sf::Vector2f dadsz = DeleteAskDialog->GetRequisition(); DeleteAskDialog->SetPosition(sf::Vector2f(wpos.x/2 - dadsz.x/2, wpos.y/2 - dadsz.y/2));
        sf::Vector2f dadfsz = DeleteFailDialog->GetRequisition(); DeleteFailDialog->SetPosition(sf::Vector2f(wpos.x/2 - dadfsz.x/2, wpos.y/2 - dadfsz.y/2));
        sf::Vector2f lswisz = SaveInfoWindow->GetRequisition(); SaveInfoWindow->SetPosition(sf::Vector2f(wpos.x - sp - lswisz.x, wpos.y - sp - lswisz.y));
        sf::Vector2f udpwsz = UDPLogoWindow->GetRequisition(); UDPLogoWindow->SetPosition(sf::Vector2f(wpos.x - sp - udpwsz.x, wpos.y - sp - udpwsz.y - lswisz.y - 10.f));
    }
}

void dSFMLTechDemo1::sMenu::sMenu::Destroy(){
    MenuWindow->Show(false); ConfigWindow->Show(false); NOOKDialog->Show(false); KeyInfoDialog->Show(false);
    LoadWindow->Show(false); DeleteAskDialog->Show(false); DeleteFailDialog->Show(false); SaveInfoWindow->Show(false);
    UDPLogoWindow->Show(false);
    desktop.RemoveAll();
    for(auto i = ControlLabelsClasses.begin(); i != ControlLabelsClasses.end(); i++) delete (*i);
    for(auto i = SaveLabelsClasses.begin(); i != SaveLabelsClasses.end(); i++) delete (*i);
    if(CurrentSave != nullptr) delete CurrentSave;
}

void dSFMLTechDemo1::sMenu::sMenu::OnGameLoadButtonClick(){
    if(CurrentSave == nullptr){
        //if no save
        CurrentSave = dSFML::Lib::SaveFile::CreateNewSave("test", g->w.capture());
        CurrentSaveRefresh();
    }
    else {
        //start game
    }
}

void dSFMLTechDemo1::sMenu::sMenu::OnLoadDialogButtonClick(){
    SaveTableRefresh(); LoadWindow->Show(true); MenuWindow->SetState(sfg::Widget::State::INSENSITIVE); desktop.BringToFront(LoadWindow); sf::Vector2u wpos = g->w.getSize();
    sf::Vector2f lwsz = LoadWindow->GetRequisition(); LoadWindow->SetPosition(sf::Vector2f(wpos.x/2 - lwsz.x/2, wpos.y/2 - lwsz.y/2));
}

void dSFMLTechDemo1::sMenu::sMenu::OnCOOPButtonClick(){}

void dSFMLTechDemo1::sMenu::sMenu::OnConfigButtonClick(){
    ConfigWindow->Show(true); NotebookConfig->SetCurrentPage(0); desktop.BringToFront(ConfigWindow);
    sf::Vector2u wpos = g->w.getSize(); sf::Vector2f cwsz = ConfigWindow->GetRequisition(); ConfigWindow->SetPosition(sf::Vector2f(wpos.x/2 - cwsz.x/2, wpos.y/2 - cwsz.y/2));
    MenuWindow->SetState(sfg::Widget::State::INSENSITIVE); g->k->RefreshBuf(); ControlsPageTableRefresh();
}

void dSFMLTechDemo1::sMenu::sMenu::OnCreditsButtonClick(){}
void dSFMLTechDemo1::sMenu::sMenu::OnQuitButtonClick(){ g->End(); }

void dSFMLTechDemo1::sMenu::sMenu::OnOKButtonClick(){ OnApplyButtonClick(); OnOKNOOKDiagClick(); }
void dSFMLTechDemo1::sMenu::sMenu::OnNOOKButtonClick(){ g->k->RefreshBuf(); sf::Vector2u wpos = g->w.getSize(); sf::Vector2f nodsz = NOOKDialog->GetRequisition(); NOOKDialog->SetPosition(sf::Vector2f(wpos.x/2 - nodsz.x/2, wpos.y/2 - nodsz.y/2)); ConfigWindow->SetState(sfg::Widget::State::INSENSITIVE); NOOKDialog->Show(true); desktop.BringToFront(NOOKDialog); }

void dSFMLTechDemo1::sMenu::sMenu::OnApplyButtonClick(){
    bool fso = FullscreenChkBox->IsActive();  bool vso = VSyncChkBox->IsActive();
    int resolutiono = ResolutionCB->GetSelectedItem(); int aao = AACB->GetSelectedItem(); int dbo = DBCB->GetSelectedItem(); int sbo = SBCB->GetSelectedItem(); int lango = LangCB->GetSelectedItem();
    float MVSv = MVS->GetAdjustment()->GetValue(); float EVSv = EVS->GetAdjustment()->GetValue(); float MuVSv = MuVS->GetAdjustment()->GetValue(); float VVSv = VVS->GetAdjustment()->GetValue();
    CSimpleIniA configini; configini.SetUnicode(true); SI_Error rc = configini.LoadFile(CONFIG_FILE_PATH);
    bool fs = configini.GetBoolValue("display", "fullscreen", DEFAULT_FS);
    if(rc > -1){
        //lang check
        CSimpleIniA lang; lang.SetUnicode(true); string l = Lang[lango]+".ini";
        string langini; bool loaded = g->PLang->GetTextFile(l, &langini); SI_Error rc;
        if (loaded) rc = lang.LoadData(langini.c_str(), langini.size());
        if (!loaded || rc < 0) dSFML::Lib::DebugStream::WriteL("FATAL ERROR: File \""+l+"\" not found! Selecting prev. language.");
        if (loaded && rc > -1) configini.SetValue("gameplay", "lang", Lang[lango].c_str());
        configini.SetLongValue("display", "width", (long)Resolutions[resolutiono][0], NULL);
        configini.SetLongValue("display", "height", (long)Resolutions[resolutiono][1], NULL);
        configini.SetLongValue("display", "bitdepth", (long)Resolutions[resolutiono][2], NULL);
        configini.SetLongValue("display", "aa", (long)AALevels[aao], NULL);
        configini.SetLongValue("display", "depthbits", (long)DBLevels[dbo], NULL);
        configini.SetLongValue("display", "stencilbits", (long)SBLevels[sbo], NULL);
        configini.SetBoolValue("display", "fullscreen", fso, NULL);
        configini.SetBoolValue("display", "vsync", vso, NULL);
        configini.SetLongValue("audio", "sound", (long)MVSv, NULL);
        configini.SetLongValue("audio", "effects", (long)EVSv, NULL);
        configini.SetLongValue("audio", "music", (long)MuVSv, NULL);
        configini.SetLongValue("audio", "voice", (long)VVSv, NULL);
        configini.SaveFile(CONFIG_FILE_PATH);
        if(rc<0)dSFML::Lib::DebugStream::WriteL("Config file error!");
        g->k->SaveBuf();
    }
    else dSFML::Lib::DebugStream::WriteL("Config file error!");
    g->w.setVerticalSyncEnabled(vso);
    if(fs == fso) g->w.setSize(sf::Vector2u(Resolutions[resolutiono][0],Resolutions[resolutiono][1]));
}

void dSFMLTechDemo1::sMenu::sMenu::OnOKNOOKDiagClick(){ NOOKDialog->Show(false); ConfigWindow->Show(false); ConfigWindow->SetState(sfg::Widget::State::NORMAL); MenuWindow->SetState(sfg::Widget::State::NORMAL); }
void dSFMLTechDemo1::sMenu::sMenu::OnNOOKNOOKDiagClick(){ NOOKDialog->Show(false); ConfigWindow->SetState(sfg::Widget::State::NORMAL); }

void dSFMLTechDemo1::sMenu::sMenu::OnLoadLoadButton(){ if(SelectedSave == nullptr) return; CurrentSave = SelectedSave; CurrentSaveRefresh(); OnLoadExitButton(); }
void dSFMLTechDemo1::sMenu::sMenu::OnLoadDeleteButton(){ if(SelectedSave != nullptr) { LoadWindow->SetState(sfg::Widget::State::INSENSITIVE); DeleteAskDialog->Show(true); desktop.BringToFront(DeleteAskDialog); } }
void dSFMLTechDemo1::sMenu::sMenu::OnLoadExitButton(){ LoadWindow->Show(false); MenuWindow->SetState(sfg::Widget::State::NORMAL); }

void dSFMLTechDemo1::sMenu::sMenu::OnOKDeleteAskDiagClick(){
    if(dSFML::Lib::SaveFile::RemoveSave(SelectedSave->FileName)){ SaveTableRefresh(); OnNODeleteAskDiagClick(); }
    else { DeleteAskDialog->SetState(sfg::Widget::State::INSENSITIVE); DeleteFailDialog->Show(true); desktop.BringToFront(DeleteFailDialog); SaveTableRefresh(); }
}
void dSFMLTechDemo1::sMenu::sMenu::OnNODeleteAskDiagClick(){ DeleteAskDialog->Show(false); desktop.BringToFront(LoadWindow); LoadWindow->SetState(sfg::Widget::State::NORMAL); }
void dSFMLTechDemo1::sMenu::sMenu::ONOKDeleteFailDiagClick(){ DeleteFailDialog->Show(false); DeleteAskDialog->Show(false); desktop.BringToFront(LoadWindow); DeleteAskDialog->SetState(sfg::Widget::State::NORMAL); LoadWindow->SetState(sfg::Widget::State::NORMAL); }

void dSFMLTechDemo1::sMenu::sMenu::MVSi(){ float val = MVS->GetAdjustment()->GetValue(); char buf[32]; sprintf(buf,"%.0f", val); MVSLabel->SetText(string(buf)); }
void dSFMLTechDemo1::sMenu::sMenu::EVSi(){ float val = EVS->GetAdjustment()->GetValue(); char buf[32]; sprintf(buf,"%.0f", val); EVSLabel->SetText(string(buf)); }
void dSFMLTechDemo1::sMenu::sMenu::MuVSi(){ float val = MuVS->GetAdjustment()->GetValue(); char buf[32]; sprintf(buf,"%.0f", val); MuVSLabel->SetText(string(buf)); }
void dSFMLTechDemo1::sMenu::sMenu::VVSi(){ float val = VVS->GetAdjustment()->GetValue(); char buf[32]; sprintf(buf,"%.0f", val); VVSLabel->SetText(string(buf)); }

bool dSFMLTechDemo1::sMenu::sMenu::IsLoaded(){ return Loaded; }
bool dSFMLTechDemo1::sMenu::sMenu::IsClosing(){ return Closing; }
string dSFMLTechDemo1::sMenu::sMenu::GetName(){ return "sMenu"; }
void dSFMLTechDemo1::sMenu::sMenu::Play_Tick_End(){ EndTick = true; }
void dSFMLTechDemo1::sMenu::sMenu::Load_Tick_End(){ EndTick = true; }

void dSFMLTechDemo1::sMenu::sMenu::ControlsPageTableRefresh(){
    auto j = ControlsButtonLabels.begin();
    auto m = g->k->GetAllKeysBuf();
    for(auto i = m->begin(); i != m->end(); i++){
        if(j == ControlsButtonLabels.end()) break;
        (*j)->SetLabel(dSFML::Lib::GetLangString(&ini, "keymap", g->k->KeyToString(i->second)));
        if(g->k->IsUsedBuf(i->second)) (*j)->SetClass("Used"); else (*j)->SetClass(" ");
        j++;
    }
}

void dSFMLTechDemo1::sMenu::sMenu::SaveTableRefresh(){
    for(auto i = SaveLabelsClasses.begin(); i != SaveLabelsClasses.end(); i++) delete (*i); SaveLabelsClasses.clear();
    LoadTable->RemoveAll(); SelectedSave = nullptr; LoadNameLabelV->SetText(""); LoadDiffLabelV->SetText(""); LoadQuestLabelV->SetText(""); LoadLvlLabelV->SetText(""); LoadImg->SetImage(LImg);
    auto SaveList = dSFML::Lib::SaveFile::GetSaves();
    for(auto i = SaveList.begin(); i != SaveList.end(); i++) {
        auto SaveL = sfg::Label::Create((*i)->SaveName); LoadTable->Attach(SaveL, sf::Rect<sf::Uint32>(0,distance(SaveList.begin(), i),1,1), 0, 0, sf::Vector2f(5.f, 5.f));
        SaveLabelC* SaveC = new SaveLabelC(*i, this, SaveL); SaveL->GetSignal(sfg::Widget::OnLeftClick).Connect(bind(&SaveLabelC::OnSignal, SaveC)); SaveLabelsClasses.push_back(SaveC);
    }
    CurrentSaveRefresh();
}

void dSFMLTechDemo1::sMenu::sMenu::CurrentSaveRefresh(){
    if(CurrentSave != nullptr) if(!dSFML::Lib::fexists(CurrentSave->Path)) { delete CurrentSave; CurrentSave = nullptr; }
    if(CurrentSave == nullptr) GameLoadButton->SetLabel(dSFML::Lib::GetLangString(&ini, "menu", "newgamebutton"));
    else GameLoadButton->SetLabel(dSFML::Lib::GetLangString(&ini, "menu", "gameloadbutton"));
    if(CurrentSave == nullptr){
        SaveInfoNameLabelV->SetText("");
        SaveInfoQuestLabelV->SetText("");
        SaveInfoDiffLabelV->SetText("");
        SaveInfoLvlLabelV->SetText("");
    }

    else {
        SaveInfoNameLabelV->SetText(CurrentSave->SaveName);
        SaveInfoQuestLabelV->SetText(dSFML::Lib::GetLangString(&(ini), "quest", CurrentSave->Quest));
        ostringstream ss; ss<<"d"<<CurrentSave->Diff;
        SaveInfoDiffLabelV->SetText(dSFML::Lib::GetLangString(&(ini), "diff", ss.str()));
        ss.str(""); ss<<CurrentSave->Level; SaveInfoLvlLabelV->SetText(ss.str());
    }
}

dSFMLTechDemo1::sMenu::ControlLabelC::ControlLabelC(string k, sMenu* s, sfg::Button::Ptr b){ Key = k; Menu = s; Button = b; }
dSFMLTechDemo1::sMenu::ControlLabelC::~ControlLabelC() { }
void dSFMLTechDemo1::sMenu::ControlLabelC::OnSignal() {
    sf::Vector2u wpos = Menu->g->w.getSize(); Menu->KeyWait = true; Menu->KeyInfoDialog->Show(true);
    sf::Vector2f kidsz = Menu->KeyInfoDialog->GetRequisition(); Menu->KeyInfoDialog->SetPosition(sf::Vector2f(wpos.x/2 - kidsz.x/2, wpos.y/2 - kidsz.y/2));
    Menu->ConfigWindow->SetState(sfg::Widget::State::INSENSITIVE); Menu->KeyButttonLabelToChange = Button; Menu->desktop.BringToFront(Menu->KeyInfoDialog); Menu->KeyToChange = Key;
}

dSFMLTechDemo1::sMenu::SaveLabelC::SaveLabelC(SaveFile* sf, sMenu* s, sfg::Label::Ptr l){ Save = sf; Menu = s; Label = l; }
dSFMLTechDemo1::sMenu::SaveLabelC::~SaveLabelC(){ delete Save; }
void dSFMLTechDemo1::sMenu::SaveLabelC::OnSignal() {
    Menu->SelectedSave = Save;
    Menu->LoadNameLabelV->SetText(Save->SaveName);
    Menu->LoadQuestLabelV->SetText(dSFML::Lib::GetLangString(&(Menu->ini), "quest", Save->Quest));
    ostringstream ss; ss<<"d"<<Save->Diff;
    Menu->LoadDiffLabelV->SetText(dSFML::Lib::GetLangString(&(Menu->ini), "diff", ss.str()));
    ss.str(""); ss<<Save->Level; Menu->LoadLvlLabelV->SetText(ss.str());
    if(Save->IsPic) Menu->LoadImg->SetImage(Save->Pic);
}
