#ifndef SMENU_H
#define SMENU_H

#include "../lib/Stage.h"
#include "../lib/log.h"
#include "../lib/exception.h"
#include "../lib/SimpleIni.h"
#include "../lib/lang.h"
#include "../lib/helper.h"
#include "../lib/save.h"
#include <SFML\Graphics.hpp>
#include <SFGUI/SFGUI.hpp>
#include <string>
#include <cstdlib>
#include <sstream>
#include <vector>
#include <windows.h>

using namespace dSFML::Lib;
using namespace std;

namespace dSFML{
    namespace Lib{
        class SaveFile;
    }
}

namespace dSFMLTechDemo1{
    namespace sMenu{
        class ControlLabelC;
        class SaveLabelC;
        //class SaveFile;
        class sMenu : public Stage{
        private:
            bool Loaded = false;
            bool Closing = false;
            bool EndTick = false;
            Game* g;
            sfg::SFGUI sfgui;
            sfg::Desktop desktop;
            sf::Clock clock;
            CSimpleIniW ini;

            //gui elements
            vector<string> Lang = {
                "pol",
                "eng"
            };
            vector<int> AALevels = { 0,2,4,8 };
            vector<int> DBLevels = { 0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32};
            vector<int> SBLevels = { 0,2,4,8,16 };
            vector<vector<int>> Resolutions = {
                { 800, 600, 32 },
                { 1024, 768, 32 },
                { 1152, 768, 32 },
                { 1280, 720, 32 },
                { 1280, 800, 32 },
                { 1280, 854, 32 },
                { 1280, 960, 32 },
                { 1280, 1024, 32 },
                { 1366, 768, 32 },
                { 1400, 1050, 32 },
                { 1440, 900, 32 },
                { 1440, 960, 32 },
                { 1440, 1024, 32 },
                { 1440, 1080, 32 },
                { 1600, 1200, 32 },
                { 1600, 1050, 32 },
                { 1680, 1050, 32 },
                { 1920, 1080, 32 },
                { 1920, 1200, 32 },
                { 800, 600, 16 },
                { 1024, 768, 16 },
                { 1152, 768, 16 },
                { 1280, 720, 16 },
                { 1280, 800, 16 },
                { 1280, 854, 16 },
                { 1280, 960, 16 },
                { 1280, 1024, 16 },
                { 1366, 768, 16 },
                { 1400, 1050, 16 },
                { 1440, 900, 16 },
                { 1440, 960, 16 },
                { 1440, 1024, 16 },
                { 1440, 1080, 16 },
                { 1600, 1200, 16 },
                { 1600, 1050, 16 },
                { 1680, 1050, 16 },
                { 1920, 1080, 16 },
                { 1920, 1200, 16 }
            };
            int MaxResW = 800; int MaxResH = 600; int MaxBitD = 16;
            sfg::Window::Ptr LoadInfo;
            sfg::Window::Ptr MenuWindow;
            sfg::Window::Ptr ConfigWindow;
            sfg::Window::Ptr NOOKDialog;
            sfg::Window::Ptr KeyInfoDialog;
            sfg::Window::Ptr LoadWindow;
            sfg::Window::Ptr DeleteAskDialog;
            sfg::Window::Ptr DeleteFailDialog;
            sfg::Window::Ptr SaveInfoWindow;
            sfg::Window::Ptr UDPLogoWindow;
            sfg::Notebook::Ptr NotebookConfig;
            sfg::ComboBox::Ptr ResolutionCB;
            sfg::CheckButton::Ptr FullscreenChkBox;
            sfg::CheckButton::Ptr VSyncChkBox;
            sfg::ComboBox::Ptr AACB;
            sfg::ComboBox::Ptr DBCB;
            sfg::ComboBox::Ptr SBCB;
            sfg::ComboBox::Ptr LangCB;
            sfg::Scale::Ptr MVS;
            sfg::Scale::Ptr EVS;
            sfg::Scale::Ptr MuVS;
            sfg::Scale::Ptr VVS;
            sfg::Label::Ptr MVSLabel;
            sfg::Label::Ptr EVSLabel;
            sfg::Label::Ptr MuVSLabel;
            sfg::Label::Ptr VVSLabel;
            sfg::Label::Ptr LoadNameLabelV;
            sfg::Label::Ptr LoadDiffLabelV;
            sfg::Label::Ptr LoadLvlLabelV;
            sfg::Label::Ptr LoadQuestLabelV;
            sfg::Image::Ptr LoadImg;
            sfg::Table::Ptr LoadTable;
            sfg::Label::Ptr SaveInfoFNameLabelV;
            sfg::Label::Ptr SaveInfoNameLabelV;
            sfg::Label::Ptr SaveInfoDiffLabelV;
            sfg::Label::Ptr SaveInfoLvlLabelV;
            sfg::Label::Ptr SaveInfoQuestLabelV;
            sfg::Button::Ptr GameLoadButton;
            sf::Image LImg;
            vector<sfg::Label::Ptr> LoadLabels;
            vector<sfg::Button::Ptr> ControlsButtonLabels;
            //vector<sfg::Label::Ptr> SaveLabels;

            //gui callbacks
            void OnGameLoadButtonClick();

            void OnLoadDialogButtonClick();
            void OnCOOPButtonClick();
            void OnConfigButtonClick();
            void OnCreditsButtonClick();
            void OnQuitButtonClick();

            void OnOKDeleteAskDiagClick();
            void OnNODeleteAskDiagClick();

            void ONOKDeleteFailDiagClick();

            void OnOKButtonClick();
            void OnNOOKButtonClick();
            void OnApplyButtonClick();

            void OnOKNOOKDiagClick();
            void OnNOOKNOOKDiagClick();

            void OnLoadLoadButton();
            void OnLoadDeleteButton();
            void OnLoadExitButton();

            void MVSi();
            void EVSi();
            void MuVSi();
            void VVSi();

            void ControlsPageTableRefresh();
            void SaveTableRefresh();
            void CurrentSaveRefresh();

            //gui vars
            bool KeyWait;
            string KeyToChange;
            sfg::Button::Ptr KeyButttonLabelToChange;
            vector<ControlLabelC*> ControlLabelsClasses;
            vector<SaveLabelC*> SaveLabelsClasses;
            SaveFile* SelectedSave = nullptr;
            SaveFile* CurrentSave = nullptr;


            //let's make frieeends!!!!
            friend class ControlLabelC;
            friend class SaveLabelC;

        public:
            sMenu();
            ~sMenu() override;

            virtual void Init(Game* g);

            virtual void Load_Tick();
            virtual void Load_Tick_End();
            virtual void Load_Draw();
            virtual void Load_Event(Event e);

            virtual void Play_Tick();
            virtual void Play_Tick_End();
            virtual void Play_Draw();
            virtual void Play_Event(Event e);

            virtual void Destroy();

            virtual bool IsLoaded();
            virtual bool IsClosing();
            virtual string GetName();
        };

        class ControlLabelC{
        private:
            string Key;
            sMenu* Menu;
            sfg::Button::Ptr Button;
        public:
            ControlLabelC(string k, sMenu* s, sfg::Button::Ptr b);
            ~ControlLabelC();
            void OnSignal();
        };

        class SaveLabelC{
        private:
            SaveFile* Save;
            sMenu* Menu;
            sfg::Label::Ptr Label;
        public:
            SaveLabelC(SaveFile* sf, sMenu* s, sfg::Label::Ptr l);
            ~SaveLabelC();
            void OnSignal();
        };
    }
}

#endif // SMENU_H


