#include <SFML\Graphics.hpp>
#include <locale>
#include <iostream>
#include "lib/console.h"
#include "lib/log.h"
#include "lib/game.h"
#include "sMenu/sMenu.h"
#include "lib/SimpleIni.h"
#include "lib/cppMemDbg.h"

using namespace sf;
using namespace dSFMLTechDemo1::sMenu;
using namespace std;

int main(int argc, char** argv)
{
    setlocale(LC_ALL, "");
    InitCPPMemDbg(MEMINFODUMP);

    vector<string> args; for(int i = 0; i < argc; i++) args.push_back(string(argv[i]));
    Game* g;

    try {
        g = new Game(args);
        g->Main();
    }
    catch(dSFML::Lib::Exception e){
        if(e.type == dSFML::Lib::Exception::EType::FileLoad) dSFML::Lib::DebugStream::WriteL("Terminating. File load error.");
        return 0;
    }
    delete g;
    cout<<"dSFMLTechDemo1 Memory status: "<<string(GetMemoryLeakInfo())<<endl;

    return 0;
}
